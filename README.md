# Sticker Finder


The Sticker Finder is a website that allows its users to trade their stickers and cards. This portal has a friendly interface that gives its users the option to search the collectibles they need to complete their collections. This website is the ideal place to find that last sticker that is making itself really hard to find. 

## Installation

All you need to do is clone this repository
```
git clone git@gitlab.com:tasguiomar/sticker-finder.git
```

## Running
Application has very few dependencies, so it’s most probably very easy to understand when you scan through the code, but there is at least few steps you should know

### Start front-end React application
Application is divided into two parts. One is pure React front-end, powered by `webpack-dev-server` in development mode.

To start this application run command below and open your app on `http://localhost:3000`

```javascript
npm start
```

To test your application, run

* `npm run test` - single run - good for CI or precook
* `npm run test:watch` - watches for changes, good for development

## Production

Running `npm run build` will create production ready application into your `dist` folder. All you need to do is make this `dist` folder publicly available.

This is good to run on your local computer to ensure, that your application is running as it should.

## Deployment

// Todo
