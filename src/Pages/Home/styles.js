import { createStyles } from '@material-ui/core';
const styles = (theme) =>
  createStyles({
    root: {
      padding: '80px 40px',
      height: '100%',
    },
    gridAlignCenter: {
      width: '100%',

      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      [theme.breakpoints.down('lg')]: {},
    },
    redirectButton: {
      width: 200,
      backgroundColor: '#0085FC',
      color: 'white',
      height: 45,
      margin: '10px 0px',
    },
    gridColumnAlignCenter: {
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',

      flexDirection: 'column',
    },
    textImageContainer: {
      display: 'flex',
      alignItems: 'center',
      [theme.breakpoints.down('lg')]: {
        flexDirection: 'column',
      },
    },
    buttonImage: {
      maxWidth: 200,
      maxHeight: 400,

      height: '100%',
    },
    mainImage: {
      position: 'absolute',
      width: '100%',
      left: '0',
      top: '25%',
    },
    mainImageContainer: {
      position: 'relative',
      maxWidth: 600,
      marginBottom: 20,
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        maxWidth: '100%',
      },
    },
  });

export default styles;
