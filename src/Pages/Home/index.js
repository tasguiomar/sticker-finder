import React from 'react';
import styles from './styles';
import { withStyles, Grid, Typography } from '@material-ui/core';

import { any } from 'prop-types';

import { withAuthorization } from '../../Services/Session';

const HomePage = ({ classes }) => {
 
  return (
    <div className={classes.root}>
      <Grid
        container
        style={{ justifyContent: 'space-betweeen', height: '100%' }}
      >
        <Grid item xs={12} className={classes.gridAlignCenter}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <div className={classes.textImageContainer}>
              <Typography variant="h2" align="center">
                Welcome to Sticker Finder
              </Typography>
            </div>
            <Typography
              variant="h3"
              align="center"
              style={{ margin: '10px 0px' }}
            >
              Start Trading Your Stickers
            </Typography>
          </div>
        </Grid>

        <Grid item xs={12} className={classes.gridAlignCenter}>
          <Grid item container className={classes.gridAlignCenter}>
            <Grid item xs={12} className={classes.gridColumnAlignCenter}>
              <div className={classes.mainImageContainer}>
                <img
                  src={require('./StickersPlayers.jpg')}
                  style={{ width: '100%' }}
                  alt={'sticker-players'}
                />
                <img
                  src={require('./logo.png')}
                  className={classes.mainImage}
                  alt={'sticker-logo'}
                />
              </div>
            </Grid>                              
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

HomePage.propTypes = {
  firebase: any,
  classes: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(withStyles(styles)(HomePage));
