import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import { TextField, Grid, Typography, withStyles } from '@material-ui/core';
import { useHistory } from 'react-router';
import { any } from 'prop-types';

import * as ROUTES from '../../Constants/routes';
import { withFirebase } from '../../Services/Firebase';
import Tabs from '../../Components/Tabs';

import Alert from '../../Components/Alert';
import PageWrapper from '../../Components/PageWrapper';

import styles from '../Login/styles';
import { withAuthorization } from '../../Services/Session';

const RegisterPage = ({ firebase, classes }) => {
  let history = useHistory();
  const [userData, setUserData] = useState({
    email: '',
    password: '',
    name: '',
  });
  const [errorMessage, setErrorMessage] = useState(null);

  const handleRegister = (event) => {
    event.preventDefault();
    firebase
      .createUserWithEmailAndPassword(userData.email, userData.password)
      .then((authUser) => {
        const user = authUser.user;
        firebase.addUserToFirestore({
          id: user.uid,
          email: user.email,
          name: userData.name,
          allowed: true,
        });
        history.push(ROUTES.HOME);
      })
      .catch((error) => {
        setErrorMessage(`${error}`);
      });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleErrorClose = () => setErrorMessage(null);

  return (
    <PageWrapper>
      <form className={classes.card} onSubmit={handleRegister}>
        <Grid
          container
          style={{ height: '100%', maxHeight: '100%', overflow: 'hidden' }}
        >
          <Grid item xs={12}>
            <Tabs />
          </Grid>
          <Grid item container xs={6} className={classes.leftGrid} spacing={2}>
            <Grid item xs={12}>
              <Typography
                className={classes.header}
                align="center"
                variant="h2"
              >
                Create an Account!
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="email"
                variant="outlined"
                className={classes.textInput}
                placeholder="Email"
                value={userData.email}
                onChange={(event) => handleChange(event)}
                type="email"
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="name"
                variant="outlined"
                className={classes.textInput}
                placeholder="Name"
                value={userData.name}
                onChange={(event) => handleChange(event)}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="password"
                variant="outlined"
                className={classes.textInput}
                placeholder="Password"
                value={userData.password}
                onChange={(event) => handleChange(event)}
                type="password"
                helperText={'Must be 8-20 characters long'}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <Button
                className={classes.confirmButton}
                variant="outlined"
                type="submit"
              >
                CONFIRM REGISTRATION
              </Button>
            </Grid>
          </Grid>
          <Grid item xs={6} className={classes.rightGrid}>
            <img
              alt={'registerImage'}
              className={classes.loginImage}
              src={
                'https://cdn62.paninicloud.com/repository/Portugal/Collectibles/images/editoriali/003719APT_1.jpg'
              }
            />
          </Grid>
        </Grid>
        <Alert
          type="error"
          message={errorMessage}
          close={true}
          handleClose={handleErrorClose}
        />
      </form>
    </PageWrapper>
  );
};

RegisterPage.propTypes = {
  firebase: any,
  classes: any,
};

const condition = (authUser) => authUser === null;

export default withAuthorization(condition)(
  withFirebase(withStyles(styles)(RegisterPage))
);
