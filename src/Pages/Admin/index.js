import {
  FormControlLabel,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  withStyles,
} from "@material-ui/core";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import AlertComponent from "../../Components/Alert";
import LoadingComponent from "../../Components/Loading";
import { withFirebase } from "../../Services/Firebase";
import styles from "./styles";

const AdminPage = ({ firebase }) => {
  const [users, setUsersList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    setLoading(true);
    firebase
      .getUsersList()
      .then((users) => {
        setUsersList(users);
      })
      .catch(() => {
        setErrorMessage("Oops, não foi possivel obter os utilizadores");
      })
      .finally(() => setLoading(false));
  }, [firebase]);

  const updateUserState = (event, uid) => {
    const { checked } = event.target;

    const actualUsers = users;

    const newUsers = users.map((user) => {
      if (user.uid === uid) {
        user.allowed = checked;
      }
      return user;
    });
    setUsersList(newUsers);
    firebase
      .updateUserState(uid, checked)
      .then(() => {})
      .catch(() => {
        setUsersList(actualUsers);
        setErrorMessage("Oops, não foi possivel alterar");
      });
  };

  return loading ? (
    <LoadingComponent />
  ) : (
    <div className="main-container">
      <div style={{ padding: "0 10px 10px" }}>
        <h1 style={{ color: "#7EA6E0" }}>{"Users"}</h1>
      </div>
      <TableContainer>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Blocked</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow key={user.id}>
                <TableCell component="th" scope="row">
                  {user.name}
                </TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={user.allowed}
                        onChange={(event) => updateUserState(event, user.uid)}
                        name="checkedA"
                        color="primary"
                      />
                    }
                    label={!user.allowed ? "Enable" : "Disable"}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <AlertComponent
        message={errorMessage}
        close={true}
        handleClose={() => setErrorMessage(null)}
      />
    </div>
  );
};

AdminPage.propTypes = {
  firebase: any,
  classes: any,
};

export default withFirebase(withStyles(styles)(AdminPage));
