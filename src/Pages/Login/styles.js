import { createStyles } from '@material-ui/core';
const styles = () =>
  createStyles({
    card: {
      width: '75%',
      height: '75%',
      backgroundColor: 'white',
      borderRadius: 10,
      border: '1px solid grey',
      alignSelf: 'cemter'
    },
    loginImage: {
      maxHeight: '100%'
    },
    leftGrid: {
      paddingTop: 30,
      display: 'flex',
      alignItems: 'center',
      height: 'fit-content'
    },
    rightGrid: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: 'calc(100% - 70px)',
      maxHeight: '100%'
    },

    forgotPassword: {
      fontSize: '80%',
      fontWeight: 400
    },
    header: {
      fontSize: 30
    },
    gridItem: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    confirmButton: {
      borderRadius: 10,
      backgroundColor: '#0085FC',
      color: 'white',
      width: '50%'
    },
    confirmButtonCollection: {
      borderRadius: 10,
      backgroundColor: '#0085FC',
      color: 'white',
      width: '80%'
    },
    textInput: {
      borderRadius: 10,
      width: '50%',
      padding: '10px 0px',
      '& .MuiOutlinedInput-root': {
        borderRadius: 10
      },
      '& .MuiOutlinedInput-input': {
        padding: '10px 5px'
      }
    },
    textInputCollection: {
      borderRadius: 10,
      width: '80%',
      padding: '10px 0px',
      '& .MuiOutlinedInput-root': {
        borderRadius: 10
      },
      '& .MuiOutlinedInput-input': {
        padding: '10px 5px'
      }
    }
  });

export default styles;
