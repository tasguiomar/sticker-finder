import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import { TextField, Grid, withStyles, Typography } from '@material-ui/core';
import { useHistory } from 'react-router';
import * as ROUTES from '../../Constants/routes';

import Alert from '../../Components/Alert';
import { withFirebase } from '../../Services/Firebase';
import { any } from 'prop-types';

import PageWrapper from '../../Components/PageWrapper';
import Tabs from '../../Components/Tabs';
import { withAuthorization } from '../../Services/Session';

import styles from './styles';

const LoginPage = ({ firebase, classes }) => {
  let history = useHistory();
  const [userData, setUserData] = useState({ email: '', password: '' });
  const [errorMessage, setErrorMessage] = useState(null);

  const handleLogin = (event) => {
    event.preventDefault();
    firebase
      .loginWithEmailAndPassword(userData.email, userData.password)
      .then(() => {
        history.push(ROUTES.HOME);
      })
      .catch((error) => {
        setErrorMessage(`${error}`);
      });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleErrorClose = () => setErrorMessage(null);

  return (
    <PageWrapper>
      <form className={classes.card} onSubmit={handleLogin}>
        <Grid
          container
          style={{ height: '100%', maxHeight: '100%', overflow: 'hidden' }}
        >
          <Grid item xs={12}>
            <Tabs />
          </Grid>{' '}
          <Grid item container xs={6} className={classes.leftGrid} spacing={2}>
            <Grid item xs={12}>
              <Typography
                variant="h2"
                align="center"
                className={classes.header}
              >
                Welcome Back!
              </Typography>{' '}
            </Grid>{' '}
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="email"
                variant="outlined"
                className={classes.textInput}
                placeholder="Email"
                value={userData.email}
                onChange={(event) => handleChange(event)}
                type="email"
              />
            </Grid>{' '}
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="password"
                variant="outlined"
                className={classes.textInput}
                placeholder="Password"
                value={userData.password}
                onChange={(event) => handleChange(event)}
                type="password"
                helperText={'Must be 8-20 characters long'}
              />{' '}
            </Grid>{' '}
            <Grid item xs={12} className={classes.gridItem}>
              <Button
                className={classes.confirmButton}
                variant="outlined"
                type="submit"
              >
                LOGIN{' '}
              </Button>{' '}
            </Grid>{' '}
            <Grid item xs={12} className={classes.gridItem}>
              <Typography className={classes.forgotPassword}>
                <a href={'/forgot-password'}> Forgot Password ? </a>{' '}
              </Typography>{' '}
            </Grid>{' '}
          </Grid>{' '}
          <Grid item xs={6} className={classes.rightGrid}>
            <img
              alt={'loginImage'}
              className={classes.loginImage}
              src={
                'https://cdn62.paninicloud.com/repository/Portugal/Collectibles/images/editoriali/003719APT_1.jpg'
              }
            />{' '}
          </Grid>{' '}
        </Grid>{' '}
        <Alert
          type="error"
          message={errorMessage}
          close={true}
          handleClose={handleErrorClose}
        />{' '}
      </form>{' '}
    </PageWrapper>
  );
};

LoginPage.propTypes = {
  firebase: any,
  classes: any,
};

const condition = (authUser) => authUser === null;

export default withAuthorization(condition)(
  withFirebase(withStyles(styles)(LoginPage))
);
