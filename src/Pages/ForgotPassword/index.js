import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import { TextField, Grid, withStyles, Typography } from '@material-ui/core';
import { useHistory } from 'react-router';
import * as ROUTES from '../../Constants/routes';

import Alert from '../../Components/Alert';
import { withFirebase } from '../../Services/Firebase';
import { any } from 'prop-types';

import PageWrapper from '../../Components/PageWrapper';
import LoadingComponent from '../../Components/Loading';
import Tabs from '../../Components/Tabs';
import { withAuthorization } from '../../Services/Session';

import styles from './styles';

const ForgotPwPage = ({ firebase, classes }) => {
  let history = useHistory();
  const [userData, setUserData] = useState({ email: '', password: '' });
  const [errorMessage, setErrorMessage] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const handleSendEmail = (event) => {
    event.preventDefault();
    setLoading(true);
    firebase
      .sendPasswordResetEmail(userData.email)
      .then(() => {
        history.push(ROUTES.LOGIN);
      })
      .catch((error) => {
        setErrorMessage(`${error}`);
      })
      .finally(() => setLoading(false));
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setUserData({ ...userData, [name]: value });
  };

  const handleErrorClose = () => setErrorMessage(null);

  return (
    <PageWrapper>
      <form className={classes.card} onSubmit={handleSendEmail}>
        {isLoading ? (
          <Grid
            container
            style={{
              height: '100%',
              maxHeight: '100%',
              overflow: 'hidden',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <LoadingComponent />
            <Typography variant="h2" align="center" className={classes.header}>
              {`Your email was sent!`}
            </Typography>
          </Grid>
        ) : (
          <Grid
            container
            style={{ height: '100%', maxHeight: '100%', overflow: 'hidden' }}
          >
            <Grid item xs={12}>
              <Tabs none />
            </Grid>{' '}
            <Grid
              item
              container
              xs={6}
              className={classes.leftGrid}
              spacing={2}
            >
              <Grid item xs={12}>
                <Typography
                  variant="h2"
                  align="center"
                  className={classes.header}
                >
                  {`Forgot your password?`}
                </Typography>
                <Typography
                  variant="h2"
                  align="center"
                  className={classes.header}
                >
                  We will send you an email!
                </Typography>
              </Grid>{' '}
              <Grid item xs={12} className={classes.gridItem}>
                <TextField
                  name="email"
                  variant="outlined"
                  className={classes.textInput}
                  placeholder="Email"
                  value={userData.email}
                  onChange={(event) => handleChange(event)}
                  type="email"
                />
              </Grid>{' '}
              <Grid item xs={12} className={classes.gridItem}>
                <Button
                  className={classes.confirmButton}
                  variant="outlined"
                  type="submit"
                >
                  SEND{' '}
                </Button>{' '}
              </Grid>{' '}
              <Grid item xs={12} className={classes.gridItem}></Grid>{' '}
            </Grid>{' '}
            <Grid item xs={6} className={classes.rightGrid}>
              <img
                alt={'loginImage'}
                className={classes.loginImage}
                src={
                  'https://cdn62.paninicloud.com/repository/Portugal/Collectibles/images/editoriali/003719APT_1.jpg'
                }
              />{' '}
            </Grid>{' '}
          </Grid>
        )}
        <Alert
          type="error"
          message={errorMessage}
          close={true}
          handleClose={handleErrorClose}
        />{' '}
      </form>{' '}
    </PageWrapper>
  );
};

ForgotPwPage.propTypes = {
  firebase: any,
  classes: any,
};

const condition = (authUser) => authUser === null;

export default withAuthorization(condition)(
  withFirebase(withStyles(styles)(ForgotPwPage))
);
