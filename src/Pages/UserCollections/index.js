// Material UI
import { Button, Card, Grid, Typography, withStyles } from "@material-ui/core";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Alert from "../../Components/Alert";
import CollectionCard from "../../Components/CollectionCard";
import * as ROUTES from "../../Constants/routes";
import { withFirebase } from "../../Services/Firebase";
import { withAuthentication, withAuthorization } from "../../Services/Session";
import EditProfile from "./EditProfile";
// Styles
import styles from "./styles";

const UserCollectionsPage = ({ firebase, classes, authUser }) => {
  let history = useHistory();
  const [collections, setCollections] = useState([]);

  const [isLoading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [userStickersList, setUserStickersList] = useState([]); //Array só com id's dos stickers do user
  const [userLikedStickers, setUserLikedStickers] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [userInfo, setUserInfo] = useState(null);

  const [userCollections, setUserCollections] = useState([]);

  const userId = authUser && authUser.uid;

  useEffect(() => {
    setLoading(true);
    userId &&
      firebase.getUserStickers(userId).then((userStickersArray) => {
        setUserStickersList(userStickersArray);
        setLoading(false);
      });

    userId && firebase.getUser(userId).then((user) => setUserInfo(user));

    userId &&
      firebase
        .getUserCollections(userId)
        .then((userCollectionsArray) => {
          userCollectionsArray.forEach((userCollection) => {
            firebase
              .getCollectionById(userCollection)
              .then(function (collection) {
                setCollections((prevState) => [
                  ...prevState,
                  { uid: collection.id, ...collection.data() },
                ]);
              });
            setUserCollections(userCollectionsArray);
          });
          setLoading(false);
        })
        .catch((error) => {
          console.error(error);
          setLoading(false);
        });
  }, [firebase, authUser, userId]);

  const addCollectionToUser = () => {
    history.push(ROUTES.COLLECTIONS);
  };

  const handleDeleteUserCollection = (event, collection) => {
    event.preventDefault();
    event.stopPropagation();
    const oldCollections = collections;
    const oldUsercollections = userCollections;

    firebase
      .deleteUserCollection(userId, collection, userCollections)
      .then(() => {
        deleteCollectionStickers(collection.uid);
        removeCollectionLikedStickers(collection.uid);
        setUserCollections(
          userCollections.filter((col) => col !== collection.uid)
        );
        setCollections(collections.filter((col) => col.uid !== collection.uid));
      })
      .catch(() => {
        setCollections(oldCollections);
        setUserCollections(oldUsercollections);
      });
  };

  const deleteCollectionStickers = (collectionToDelete) => {
    firebase
      .getStickersByCollection(collectionToDelete)
      .then((stickersDocs) => {
        const collectionToDeleteStickers = [];
        stickersDocs.forEach((stickerDoc) => {
          collectionToDeleteStickers.push(stickerDoc.id);
        });

        removeCollectionStickers(collectionToDeleteStickers);

        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setErrorMessage("Oops, error removing the stickers!");
        setLoading(false);
      });
  };

  const removeCollectionStickers = (collectionStickersToDelete) => {
    const updatedList = userStickersList.filter(function (el) {
      return !collectionStickersToDelete.includes(el);
    });

    const oldList = userStickersList;

    setUserLikedStickers(updatedList);

    firebase
      .deleteUserSticker(userId, updatedList)
      .then(() => {})
      .catch(() => setUserStickersList(oldList));
  };

  const removeCollectionLikedStickers = (collectionId) => {
    firebase
      .getStickersByCollection(collectionId)
      .then((stickersDocs) => {
        const stickersFromCollection = [];
        stickersDocs.forEach((stickerDoc) => {
          stickersFromCollection.push(stickerDoc.id);
        });
        deleteCollectionLikedStickers(stickersFromCollection);
        setLoading(false);
      })
      .catch(() => {
        setErrorMessage("Oops, error getting the stickers!");
        setLoading(false);
      });
  };

  const deleteCollectionLikedStickers = (stickersFromCollection) => {
    const toDeleteList = [];
    stickersFromCollection.forEach((collectionSticker) => {
      userLikedStickers.forEach((userSticker) => {
        if (collectionSticker === userSticker.uid) {
          toDeleteList.push(userSticker);
        }
      });
    });

    toDeleteList.forEach((toDelete) => {
      firebase.removeFavSticker(toDelete.favId).catch((error) => {
        setErrorMessage("Oops, error unliking a sticker!");
        console.error(error);
      });
    });
  };

  function ListCollections() {
    return collections.map((collection) => {
      const handleClick = (event) => {
        event.preventDefault();
        history.push(
          ROUTES.SELECTSTICKERS.replace(":collectionId", collection.uid)
        );
      };
      return (
        <Grid
          key={collection.uid}
          item
          xs={12}
          md={6}
          lg={3}
          onClick={handleClick}
        >
          <CollectionCard
            handleClick={handleClick}
            collection={collection}
            isUser={true}
            deleteCollection={(event) =>
              handleDeleteUserCollection(event, collection)
            }
          />
        </Grid>
      );
    });
  }

  const handleEditMode = (value, userNewInfo) => {
    setIsEditing(value);
    setUserInfo(userNewInfo);
  };

  return !userInfo ? (
    <div></div>
  ) : (
    <div className={[classes.headerContainer, "main-container"].join(" ")}>
      <h1 className={classes.title}>
        My Profile &nbsp;{" "}
        <Button
          className={classes.addButton}
          size="small"
          variant="outlined"
          onClick={() => setIsEditing(!isEditing)}
        >
          Edit Profile
        </Button>
      </h1>

      {isEditing ? (
        <EditProfile handleEditMode={handleEditMode} profileInfo={userInfo} />
      ) : (
        <Card>
          <br />
          <br />
          <Grid container spacing={2}>
            <Grid item lg={4} xs={12}>
              <div
                className={[classes.gridColumn, classes.gridImage].join(" ")}
              >
                <img
                  alt={"my-profile"}
                  className={classes.imageContainer}
                  src={userInfo ? userInfo.image : ""}
                />
              </div>
            </Grid>
            <Grid item container lg={8} xs={12} spacing={2}>
              <Grid item xs={6} className={classes.gridColumn}>
                <Typography
                  variant="body2"
                  className={classes.title}
                  component="p"
                >
                  Name:
                </Typography>
                {<div>{userInfo ? userInfo.name : ""}</div>}
              </Grid>
              <Grid item xs={6} className={classes.gridColumn}>
                <Typography
                  variant="body2"
                  className={classes.title}
                  component="p"
                >
                  Email:
                </Typography>
                {<div>{userInfo ? userInfo.email : ""}</div>}
              </Grid>
              <Grid item xs={6} className={classes.gridColumn}>
                <Typography
                  variant="body2"
                  className={classes.title}
                  component="p"
                >
                  Telemóvel:
                </Typography>
                <div>{userInfo ? userInfo.telemovel : ""}</div>
              </Grid>
              <Grid item xs={6} className={classes.gridColumn}>
                <Typography
                  variant="body2"
                  className={classes.title}
                  component="p"
                >
                  Descrição:
                </Typography>
                {<div>{userInfo ? userInfo.descricao : ""}</div>}
              </Grid>
            </Grid>
          </Grid>
          <br />
          <br />
        </Card>
      )}
      <h1 className={classes.title}>
        My Collections &nbsp;
        <Button
          className={classes.addButton}
          size="small"
          onClick={addCollectionToUser}
          variant="outlined"
        >
          ADD COLLECTION
        </Button>
      </h1>
      <h4>
        Tip: These are your collections, you can click them to see its stickers
        and select the ones you have already, or you can add the ones you want
        to your wish list!
      </h4>
      {!isLoading && (
        <Grid container spacing={4}>
          {collections.length > 0 ? (
            ListCollections()
          ) : (
            <div className={classes.emptyCollection}>
              <Typography>You have no collections yet!</Typography>
            </div>
          )}
        </Grid>
      )}
      <Alert
        type="error"
        message={errorMessage}
        close={true}
        handleClose={() => setErrorMessage(null)}
      />
    </div>
  );
};

UserCollectionsPage.propTypes = {
  firebase: any,
  classes: any,
  authUser: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withAuthentication(withFirebase(withStyles(styles)(UserCollectionsPage)))
);
