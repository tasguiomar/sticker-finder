import { createStyles } from '@material-ui/core';
const blue = '#7EA6E0';
const styles = () =>
  createStyles({
    confirmButtonContainer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: '10px 20px 5px',
    },
    imageContainer: {
      objectFit: 'cover',
      borderRadius: '50%',
      height: '200px',
      width: '200px',
      marginLeft: '100px',
    },
    title: {
      color: blue,
    },
    textInput: {
      borderRadius: 10,
      width: '100%',
      '& .MuiOutlinedInput-root': {
        borderRadius: 10,
      },
    },
    confirmButton: {
      width: '100%',
      display: 'flex',
      alignSelf: 'flex-end',
      maxWidth: 200,
      '&:not(:disabled)': {
        borderRadius: 10,
        backgroundColor: '#0085FC',
        color: 'white',
      },
    },
  });

export default styles;
