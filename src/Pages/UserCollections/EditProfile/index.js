import React, { useState } from "react";

import Button from "@material-ui/core/Button";
import {
  TextField,
  Grid,
  withStyles,
  Card,
  Typography,
} from "@material-ui/core";

import { withFirebase } from "../../../Services/Firebase";
import { any } from "prop-types";

import styles from "./styles";
import ImageUploader from "../../../Components/ImageUploader";
import SaveIcon from "@material-ui/icons/Save";
import Alert from "@material-ui/lab/Alert";

const EditProfile = ({ profileInfo, firebase, classes, handleEditMode }) => {
  const [profileData, setProfileData] = useState(profileInfo);

  const [errorMessage, setErrorMessage] = useState(null);

  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setProfileData({ ...profileData, [name]: value });
  };

  const handleEditProfile = (event) => {
    event.preventDefault();
    const obj = { ...profileData };

    firebase
      .updateProfile(profileData.id, obj)
      .then(() => {
        handleEditMode(false, profileData);
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const handleImageUpload = (url) => {
    setProfileData({ ...profileData, image: url });
  };

  return (
    <div>
      <form onSubmit={handleEditProfile}>
        <div className={classes.headerContainer}>
          <Card>
            <br />
            <br />
            <Grid container spacing={2}>
              <Grid item lg={3} xs={12}>
                <div>
                  <ImageUploader
                    handleImageUpload={handleImageUpload}
                    image={profileData.image}
                  />
                </div>
              </Grid>
              <Grid item container lg={9} xs={12} spacing={2}>
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.title}
                    component="p"
                  >
                    Name:
                  </Typography>
                  <TextField
                    name="name"
                    variant="outlined"
                    className={classes.textInput}
                    placeholder="Name"
                    value={profileData.name}
                    onChange={(event) => handleChange(event)}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.title}
                    component="p"
                  >
                    Email:
                  </Typography>
                  <TextField
                    name="email"
                    variant="outlined"
                    className={classes.textInput}
                    placeholder="Email"
                    value={profileData.email}
                    onChange={(event) => handleChange(event)}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.title}
                    component="p"
                  >
                    Telemóvel:
                  </Typography>
                  <TextField
                    name="telemovel"
                    variant="outlined"
                    inputProps={{ maxLength: 9, minLength: 9 }}
                    className={classes.textInput}
                    placeholder="Telemóvel"
                    value={profileData.telemovel}
                    onChange={(event) => handleChange(event)}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    variant="body2"
                    className={classes.title}
                    component="p"
                  >
                    Descrição:
                  </Typography>
                  <TextField
                    name="descricao"
                    variant="outlined"
                    multiline
                    inputProps={{ maxLength: 200 }}
                    className={classes.textInput}
                    placeholder="Descrição"
                    value={profileData.descricao}
                    onChange={(event) => handleChange(event)}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "flex-end",
                  }}
                >
                  <Button type="submit" className={classes.confirmButton}>
                    <SaveIcon title="Save Profile" />
                    &nbsp; Save
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <br />
            <br />
          </Card>
        </div>
      </form>
      {errorMessage && (
        <Alert
          type="error"
          message={errorMessage}
          close={true}
          handleClose={() => setErrorMessage(null)}
        />
      )}
    </div>
  );
};

EditProfile.propTypes = {
  firebase: any,
  classes: any,
  profile: any,
  handleClose: any,
  handleEditMode: any,
  profileInfo: any,
};

export default withFirebase(withStyles(styles)(EditProfile));
