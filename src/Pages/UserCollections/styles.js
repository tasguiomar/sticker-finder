import { createStyles } from "@material-ui/core";
const blue = "#7EA6E0";
const styles = (theme) =>
  createStyles({
    title: {
      color: blue,
    },
    addButton: {
      color: blue,
    },
    field: {
      color: blue,
    },
    emptyCollection: {
      width: "100%",
      display: "flex",
      alignItems: "center",
      height: 200,
      justifyContent: "center",
    },
    imageContainer: {
      objectFit: "cover",
      borderRadius: "50%",
      height: "200px",
      width: "200px",
    },
    gridColumn: {
      [theme.breakpoints.down("lg")]: {
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
      },
    },
    gridImage: {
      [theme.breakpoints.up("lg")]: { marginLeft: "100px" },
    },
  });

export default styles;
