import { createStyles } from '@material-ui/core';
const blue = '#7EA6E0';
const styles = (theme) =>
  createStyles({
    headerContainer: {
      padding: '0 10px 10px',
    },
    title: {
      color: blue,
    },
    addButton: {
      color: blue,
    },
    field: {
      color: blue,
    },
    emptyCollection: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      height: 200,
      justifyContent: 'center',
    },
    imageContainer: {
      objectFit: 'cover',
      borderRadius: '50%',
      height: '70px',
      width: '70px',
    },
    formContainer: {
      background: '#CCE5FF',
      height: '100%',
      padding: '2px',
      position: 'relative',
      overflow: 'auto',
    },
    formWrapper: {
      background: '#FFFFFF',
      borderRadius: '10px',
      boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
      maxWidth: '1200px',
      width: '80%',
      padding: '5px 40px 40px',
      margin: '40px auto',
    },
    gridColumn: {
      [theme.breakpoints.down('lg')]: {
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column',
      },
    },
    gridImage: {
      [theme.breakpoints.up('lg')]: { marginLeft: '100px' },
    },
  });

export default styles;
