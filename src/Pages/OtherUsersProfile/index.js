import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  withStyles,
} from '@material-ui/core';
import { any } from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import AlertComponent from '../../Components/Alert';
import LoadingComponent from '../../Components/Loading';
import * as ROUTES from '../../Constants/routes';
import { withFirebase } from '../../Services/Firebase';
import styles from './styles';

const OtherUsersProfile = ({ firebase, classes }) => {
  let history = useHistory();
  const [users, setUsersList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    setLoading(true);
    firebase
      .getUsersList()
      .then((users) => {
        setUsersList(users);
      })
      .catch(() => {
        setErrorMessage('Oops, não foi possivel obter os utilizadores');
      })
      .finally(() => setLoading(false));
  }, [firebase]);

  const Userprofile = (announcerInfo) => {
    history.push({
      pathname: ROUTES.OTHERUSERSPROFILEDETAILS,
      state: announcerInfo,
    });
  };

  return loading ? (
    <LoadingComponent />
  ) : (
    <div className="main-container">
      <div style={{ padding: '0 10px 10px' }}>
        <h1 style={{ color: '#7EA6E0' }}>{'All Users'}</h1>
      </div>
      <TableContainer>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Phone Number</TableCell>
              <TableCell>Description</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow key={user.id} style={{ height: 10 }}>
                <TableCell>
                  {' '}
                  <img
                    className={classes.imageContainer}
                    src={user ? user.image : ''}
                    alt="other-user"
                  />
                </TableCell>
                <TableCell component="th" scope="row">
                  {user.name || '-'}
                </TableCell>
                <TableCell>{user.email || '-'}</TableCell>
                <TableCell>{user.telemovel || '-'}</TableCell>
                <TableCell>{user.descricao || '-'}</TableCell>
                <TableCell>
                  <Button
                    className={classes.addButton}
                    variant="outlined"
                    type="submit"
                    onClick={() => Userprofile(user.id)}
                  >
                    View user details
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <AlertComponent
        message={errorMessage}
        close={true}
        handleClose={() => setErrorMessage(null)}
      />
    </div>
  );
};

OtherUsersProfile.propTypes = {
  firebase: any,
  classes: any,
};

export default withFirebase(withStyles(styles)(OtherUsersProfile));
