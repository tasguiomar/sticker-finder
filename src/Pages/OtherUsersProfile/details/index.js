// React
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router";
import { withFirebase } from "../../../Services/Firebase";
import { withStyles, IconButton } from "@material-ui/core";
import { TextField, Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import styles from "./styles";
import LoadingComponent from "../../../Components/Loading";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  withAuthentication,
  withAuthorization,
} from "../../../Services/Session";
import { any } from "prop-types";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
const blue = "#7EA6E0";
const OtherUsersProfileDetails = ({ firebase, classes, authUser }) => {
  let history = useHistory();
  const userId = history.location.state;
  const [userInfo, setUserInfo] = useState(null);
  const [commantInfo, setCommantInfo] = useState("");
  const [commentTextInfo, setCommentTextInfo] = useState("");
  const [usersList, setUsersList] = useState([]);

  const activeUserId = authUser && authUser.uid;

  const obj = {
    comment: "guiomar",
  };

  const handleChange = (event) => {
    event.preventDefault();
    const { value } = event.target;

    setCommentTextInfo(value);
  };

  const getCommentsInfo = () => {
    const array = [];
    firebase.getCommantsId(userId).then((commentsDb) => {
      commentsDb.forEach(function (doc) {
        array.push({ uid: doc.id, ...doc.data() });
      });

      setCommantInfo(array ? array : []);
    });
  };

  const deleteComment = (commentId) => {
    firebase.deleteComment(commentId).then(() => {
      getCommentsInfo();
    });
  };

  const handleSaveComment = () => {
    obj.idReceiver = userId;
    obj.comment = commentTextInfo;
    obj.idSender = activeUserId;
    obj.date = new Date().toLocaleDateString("en-GB");
    if (commentTextInfo) {
      firebase.saveComment(obj).then(() => {
        getCommentsInfo();
        setCommentTextInfo("");
      });
    }
  };

  const getUserName = (uid) => {
    console.log(usersList);
    let userName = "";

    usersList.forEach((user) => {
      if (user.uid === uid) {
        userName = user.name;
      }
    });

    return userName;
  };
  // teste
  useEffect(() => {
    firebase.getUser(userId).then((user) => setUserInfo(user));
    getCommentsInfo();

    firebase.getUsersList().then((users) => {
      setUsersList(users);
    });
    // eslint-disable-next-line
  }, [firebase, userId]);

  return userInfo ? (
    <div className={classes.formContainer}>
      <div className={classes.formWrapper}>
        <h1>{`${userInfo.name}'s Profile Details`}</h1>
        <Grid container spacing={2}>
          <Grid item lg={4} xs={12}>
            <div className={[classes.gridColumn, classes.gridImage].join(" ")}>
              <img
                alt={"my-profile"}
                className={classes.imageContainer}
                src={userInfo ? userInfo.image : ""}
              />
            </div>
          </Grid>
          <Grid item container lg={8} xs={12} spacing={2}>
            <Grid item xs={6} className={classes.gridColumn}>
              <Typography variant="h6" className={classes.title} component="p">
                Name:
              </Typography>
              {<div>{userInfo ? userInfo.name : ""}</div>}
            </Grid>
            <Grid item xs={6} className={classes.gridColumn}>
              <Typography variant="h6" className={classes.title} component="p">
                Email:
              </Typography>
              {<div>{userInfo ? userInfo.email : ""}</div>}
            </Grid>
            <Grid item xs={6} className={classes.gridColumn}>
              <Typography variant="h6" className={classes.title} component="p">
                Phone Number:
              </Typography>
              <div>{userInfo ? userInfo.telemovel : ""}</div>
            </Grid>
            <Grid item xs={6} className={classes.gridColumn}>
              <Typography variant="h6" className={classes.title} component="p">
                Description:
              </Typography>
              {<div>{userInfo ? userInfo.descricao : ""}</div>}
            </Grid>
          </Grid>
        </Grid>
        <br />
        <br />
      </div>
      <div className={classes.formWrapper}>
        <h1>Comments Section</h1>
        <h4>
          Tip: In here you can write something about the users you have been in
          contact with, please review with honesty!{" "}
        </h4>
        <Grid container>
          <Grid item xs={9}>
            <TextField
              name="comment"
              multiline
              variant="outlined"
              className={classes.textfield}
              placeholder="Comment"
              value={commentTextInfo}
              onChange={(event) => handleChange(event)}
            />
          </Grid>
          <Grid item xs={3}>
            <Button
              className={classes.confirmButtonCollection}
              variant="outlined"
              type="submit"
              style={{ color: blue }}
              onClick={() => handleSaveComment()}
            >
              Send
            </Button>
          </Grid>
        </Grid>
        <TableContainer style={{ marginTop: 20 }}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>User</TableCell>
                <TableCell>Comment</TableCell>
                <TableCell>Date</TableCell>
                <TableCell>Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {commantInfo &&
                commantInfo.map((comd) => (
                  <TableRow key={comd.uid}>
                    <TableCell>{getUserName(comd.idSender)}</TableCell>
                    <TableCell component="th" scope="row">
                      {comd.comment}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {comd.date ? comd.date : ""}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {comd.idSender === activeUserId && (
                        <IconButton
                          aria-label="delete"
                          onClick={() => deleteComment(comd.uid)}
                        >
                          <DeleteIcon />
                        </IconButton>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <br />
        <br />
      </div>
    </div>
  ) : (
    <LoadingComponent />
  );
};

OtherUsersProfileDetails.propTypes = {
  firebase: any,
  classes: any,
  authUser: any,
};

export default withAuthorization()(
  withAuthentication(withFirebase(withStyles(styles)(OtherUsersProfileDetails)))
);
