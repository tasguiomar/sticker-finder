import { createStyles } from '@material-ui/core';
const blue = '#7EA6E0';
const styles = (theme) =>
  createStyles({
    confirmButtonCollection: {
      height: 55,
      marginLeft: 20,
      width: '100%',
    },
    headerContainer: {
      padding: '0 10px 10px',
    },
    title: {
      color: blue,
    },
    addButton: {
      color: blue,
    },
    field: {
      color: blue,
    },
    emptyCollection: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      height: 200,
      justifyContent: 'center',
    },
    imageContainer: {
      objectFit: 'cover',
      borderRadius: '50%',
      height: '200px',
      width: '200px',
    },
    formContainer: {
      background: '#CCE5FF',
      height: '100%',
      padding: '2px',
      position: 'relative',
      overflow: 'auto',
    },
    formWrapper: {
      background: '#FFFFFF',
      borderRadius: '10px',
      boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
      maxWidth: '1200px',
      width: '80%',
      padding: '5px 40px 40px',
      margin: '40px auto',
    },
    commentWrapper: {
      background: '#FFFFFF',
      borderRadius: '10px',
      boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
      maxWidth: '1200px',
      padding: '5px 40px 40px',
      margin: '40px auto',
      width: '100%',
    },
    gridColumn: {
      [theme.breakpoints.down('lg')]: {
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column',
      },
    },
    gridImage: {
      [theme.breakpoints.up('lg')]: { marginLeft: '100px' },
    },
    textfield: {
      width: '100%',
    },
  });

export default styles;
