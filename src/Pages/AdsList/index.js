// Material UI
import { Grid, Typography, withStyles } from "@material-ui/core";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Alert from "../../Components/Alert";
import CollectionCard from "../../Components/CollectionCard";
import ConfirmationModal from "../../Components/ConfirmationModal";
import FiltersBar from "../../Components/FiltersBar";
import ListHeader from "../../Components/ListHeader";
import LoadingComponent from "../../Components/Loading";
import * as ROUTES from "../../Constants/routes";
import { withFirebase } from "../../Services/Firebase";
import { withAuthentication, withAuthorization } from "../../Services/Session";
// Styles
import styles from "./style";

const AdsPage = ({ firebase, classes, authUser }) => {
  let history = useHistory();
  const [filtersData, setFiltersData] = useState(null);
  const [collections, setCollections] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [deleteSelectedCollectionId, setDeleteSelectedCollectionId] = useState(
    null
  );

  const [userInfo, setUserInfo] = useState(false);
  const userId = authUser && authUser.uid;

  useEffect(() => {
    const array = [];
    setLoading(true);
    userId &&
      firebase.getUser(userId).then((user) => {
        setUserInfo(user);
      });
    firebase
      .getAds(filtersData)
      .then(function (collectionsDb) {
        collectionsDb.forEach(function (doc) {
          array.push({ uid: doc.id, ...doc.data() });
        });
        setCollections(array);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
        setErrorMessage("Oops, não foi possivel obter as coleções");
      });
  }, [firebase, filtersData, userId]);

  const onDeleteSelectedCollectionTrue = () => {
    if (deleteSelectedCollectionId) {
      firebase
        .deleteAds(deleteSelectedCollectionId)
        .then(() => {
          setCollections(
            collections.filter(
              (collection) => collection.uid !== deleteSelectedCollectionId
            )
          );
        })
        .catch(() => {
          setErrorMessage("Oops, não foi possivel apagar a coleção");
        });
      setDeleteSelectedCollectionId(null);
    }
  };

  const onDeleteSelectedCollectionFalse = () => {
    setDeleteSelectedCollectionId(null);
  };

  const deleteSelectedCollection = (uidToDelete) => {
    setDeleteSelectedCollectionId(uidToDelete);
  };

  const addCollection = () => {
    history.push(ROUTES.ADSFORM.replace(":adId?", ""));
  };

  const editCollection = (collection) => {
    history.push({
      pathname: ROUTES.ADSFORM.replace(":adId?", collection.uid),
      state: collection.uid,
    });
  };

  function ListCollections() {
    return collections.map((ad) => {
      const handleClick = (event) => {
        event.preventDefault();
        history.push({
          pathname: ROUTES.ADINFO,
          state: ad.uid,
        });
      };
      return (
        <Grid key={ad.uid} item xs={12} md={6} lg={3}>
          <CollectionCard
            handleClick={handleClick}
            collection={ad}
            deleteCollection={deleteSelectedCollection}
            editCollection={editCollection}
            isAllowedToEdit={userInfo.id === ad.userId}
          />
        </Grid>
      );
    });
  }

  return (
    <div className="main-container">
      <ListHeader title="Ads" handleAdd={addCollection} allowAll />
      <FiltersBar type="ad" handleFilter={setFiltersData} />
      {isLoading || !userInfo ? (
        <LoadingComponent />
      ) : (
        <>
          <h4>
            Tip: These are our available adds, if you see something you would
            like to have you can click an add and contact its announcer!
          </h4>
          <Grid container spacing={4} className={classes.collectionsList}>
            {collections.length > 0 ? (
              ListCollections()
            ) : (
              <div className="empty-message">
                <Typography>No ads available!</Typography>
              </div>
            )}
          </Grid>
        </>
      )}
      <Alert
        type="error"
        message={errorMessage}
        close={true}
        handleClose={() => setErrorMessage(null)}
      />
      {deleteSelectedCollectionId && (
        <ConfirmationModal
          onTrue={onDeleteSelectedCollectionTrue}
          onFalse={onDeleteSelectedCollectionFalse}
        />
      )}
    </div>
  );
};

AdsPage.propTypes = {
  firebase: any,
  classes: any,
  authUser: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withAuthentication(withFirebase(withStyles(styles)(AdsPage)))
);
