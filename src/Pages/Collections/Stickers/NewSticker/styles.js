import { createStyles } from '@material-ui/core';
const styles = () =>
  createStyles({
    collectionsContainer: {
      padding: '0 40px',
    },

    gridItem: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textInput: {
      borderRadius: 10,
      width: '100%',
      padding: '10px 0px',
      '& .MuiOutlinedInput-root': {
        borderRadius: 10,
      },
      '& .MuiOutlinedInput-input': {
        padding: '10px 5px',
      },
    },
    listActions: {
      display: 'flex',
      height: '100%',
      justifyContent: 'center',
      padding: '10px',
      position: 'absolute',
      bottom: '0',
      right: '0',
      '& > svg': {
        cursor: 'pointer',
        fontSize: '1.8rem',
      },
    },
    stickerItem: {
      margin: '2px auto 1px',
      padding: '20px 10px',
      overflow: 'hidden',
      position: 'relative',
      width: '100%',
    },
  });

export default styles;
