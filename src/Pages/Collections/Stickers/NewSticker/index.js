/* eslint-disable no-undef */
import React, { useState } from 'react';

//import Button from '@material-ui/core/Button';
import { TextField, Grid, withStyles, Card } from '@material-ui/core';
import { withFirebase } from '../../../../Services/Firebase';

import { any } from 'prop-types';

import Alert from '../../../../Components/Alert';
import styles from './styles';
import ImageUploader from '../../../../Components/ImageUploader';
import CancelIcon from '@material-ui/icons/Cancel';
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';

const NewSticker = ({
  firebase,
  classes,
  collectionId,
  handleClose,
  updateStickersList,
}) => {
  const [stickerData, setStickerData] = useState({
    collectionId,
    name: '',
    code: '',
    description: '',
  });
  const [errorMessage, setErrorMessage] = useState(null);

  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setStickerData({ ...stickerData, [name]: value });
  };

  const handleAddNewSticker = (event) => {
    event.preventDefault();
    firebase.addSticker(collectionId, stickerData).then(() => {
      handleClose();
      updateStickersList();
    });
  };

  const handleErrorClose = () =>
    setErrorMessage("Oops, we couldn't add this sticker!");

  const handleImageUpload = (url) => {
    setStickerData({ ...stickerData, image: url });
  };

  return (
    <form onSubmit={handleAddNewSticker}>
      <Card className={classes.stickerItem}>
        <Grid container spacing={2} className={classes.row}>
          <Grid item xs={3} className={classes.gridItem}>
            <div className={classes.imageContainer}>
              <ImageUploader handleImageUpload={handleImageUpload} />
            </div>
          </Grid>
          <Grid item xs={2} className={classes.gridItem}>
            <TextField
              name="name"
              variant="outlined"
              className={classes.textInput}
              placeholder="Sticker Name"
              value={stickerData.name}
              onChange={(event) => handleChange(event)}
            />
          </Grid>
          <Grid item xs={2} className={classes.gridItem}>
            <TextField
              name="code"
              variant="outlined"
              className={classes.textInput}
              placeholder="Sticker Code"
              value={stickerData.code}
              onChange={(event) => handleChange(event)}
            />
          </Grid>
          <Grid item xs={3} className={classes.gridItem}>
            <TextField
              name="description"
              variant="outlined"
              className={classes.textInput}
              placeholder="Description"
              value={stickerData.description}
              onChange={(event) => handleChange(event)}
            />
          </Grid>
          <Grid item xs={2} className={classes.gridItem}>
            <div className={classes.listActions}>
              <Button type="submit">
                <SaveIcon title="Save Sticker" />
              </Button>
              <Button onClick={() => handleClose()}>
                <CancelIcon title="Cancel Sticker" />
              </Button>
            </div>
          </Grid>
          <Alert
            type="error"
            message={errorMessage}
            close={true}
            handleClose={handleErrorClose}
          />
        </Grid>
      </Card>
    </form>
  );
};

NewSticker.propTypes = {
  firebase: any,
  classes: any,
  collectionId: any,
  handleClose: any,
  updateStickersList: any,
};

export default withFirebase(withStyles(styles)(NewSticker));
