import { createStyles } from "@material-ui/core";
const styles = (theme) =>
  createStyles({
    gridItem: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      padding: "10px",
    },
    stickerItem: {
      margin: "2px auto 1px",
      padding: "20px 10px",
      overflow: "hidden",
      position: "relative",
      width: "100%",
    },
    listActions: {
      display: "flex",
      justifyContent: "flex-end",
      position: "absolute",
      right: "0",
      "& > svg": {
        cursor: "pointer",
        fontSize: "1.8rem",
      },
    },
    [theme.breakpoints.between("sm", "md")]: {
      stickersList: {
        display: "grid",
        gridTemplateColumns: "1fr 1fr",
        gridTemplateRows: "auto",
        columnGap: "12px",
        rowGap: "12px",
      },
    },
  });
export default styles;
