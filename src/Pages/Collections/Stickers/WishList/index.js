// Material
import { withStyles } from "@material-ui/core";
// React
import { any } from "prop-types";
import React from "react";
// Local
import StickersList from "../../../../Components/StickersList";
import { withFirebase } from "../../../../Services/Firebase";
import {
  withAuthentication,
  withAuthorization,
} from "../../../../Services/Session";
import {
  appUseEffect,
  appUseState,
} from "../../../../Services/Utils/react-hooks";
import styles from "./styles";

const WishListPage = ({ firebase, classes, match, authUser }) => {
  const [collectionId] = appUseState(match.params.collectionId);
  const [loadingState, setLoadingState] = appUseState(true);
  const [errorMessage, setErrorMessage] = appUseState(null);
  const [userLikedStickers, setUserLikedStickers] = appUseState([]);
  const [isWishListPage, setIsWishListPage] = appUseState(true);

  const userId = authUser && authUser.uid;

  appUseEffect(() => {
    setLoadingState(true);
    setIsWishListPage(true);

    userId &&
      firebase
        .getFavStickers(userId)
        .then((stickersFavDb) => {
          const stickersFavArray = [];
          let i = 0;
          const end = stickersFavDb.docs.length - 1;
          stickersFavDb &&
            stickersFavDb.forEach((stickerFav) => {
              const stickerFavData = stickerFav.data();
              stickerFavData &&
                firebase
                  .getStickerById(stickerFavData.sticker)
                  .then((stickerDb) => {
                    stickerDb &&
                      stickersFavArray.push({
                        favId: stickerFav.id,
                        uid: stickerDb.id,
                        ...stickerDb.data(),
                      });
                    if (i === end) {
                      setUserLikedStickers(stickersFavArray);
                      setLoadingState(false);
                    }
                    i++;
                  });
            });
          setLoadingState(false);
        })
        .catch(() => {
          setErrorMessage("Oops, error getting the stickers!");
          setLoadingState(false);
        })
        .finally(() => setLoadingState(false));
  }, [firebase, authUser, userId]);

  const handleItemClick = () => () => {};
  const handleEditClick = () => () => {};
  const handleCloseEdit = () => () => {};
  const handleDelClick = () => () => {};
  const hasAlreadySticker = () => () => {};
  const handleAddUserSticker = () => () => {};

  const handleLikeSticker = (stickerId) => () => {
    if (authUser) {
      firebase.favSticker(authUser.uid, stickerId).catch((error) => {
        setErrorMessage("Oops, error liking a sticker!");
        console.error(error);
      });
    }
  };

  const handleDislikeSticker = (favId) => {
    const updatedList = [];

    userLikedStickers.forEach((likedSticker) => {
      if (likedSticker.favId !== favId) {
        updatedList.push(likedSticker);
      }
    });

    firebase
      .removeFavSticker(favId)
      .then(() => setUserLikedStickers(updatedList))
      .catch((error) => {
        setErrorMessage("Oops, error unliking a sticker!");
        console.error(error);
      });
  };

  const handleDeleteUserSticker = () => () => {};

  return (
    <div className="main-container">
      <h1 className={classes.title}>Wish List &nbsp;</h1>
      <h4>
        {
          "Tip: These are the stickers you want to have! If an ad is created you will be notified about that sticker's availability"
        }
      </h4>
      <StickersList
        stickers={userLikedStickers}
        handleItemClick={handleItemClick}
        handleEditClick={handleEditClick}
        handleDelClick={handleDelClick}
        error={errorMessage}
        loading={loadingState}
        collectionId={collectionId}
        handleCloseEdit={handleCloseEdit}
        updateStickersList={userLikedStickers}
        handleAddUserSticker={handleAddUserSticker}
        handleDeleteUserSticker={handleDeleteUserSticker}
        handleLikeSticker={handleLikeSticker}
        handleDislikeSticker={handleDislikeSticker}
        isUserStickersPage={false}
        addButtonOn={hasAlreadySticker}
        isWishListPage={isWishListPage}
      />
    </div>
  );
};

WishListPage.propTypes = {
  firebase: any,
  classes: any,
  history: any,
  match: any,
  authUser: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withAuthentication(withFirebase(withStyles(styles)(WishListPage)))
);
