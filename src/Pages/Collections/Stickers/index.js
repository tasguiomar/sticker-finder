// Material
import { Typography, withStyles } from "@material-ui/core";
// React
import { any } from "prop-types";
import React, { useState } from "react";
import { withRouter } from "react-router";
import ConfirmationModal from "../../../Components/ConfirmationModal";
import FiltersBar from "../../../Components/FiltersBar";
// Local
import ListHeader from "../../../Components/ListHeader";
import StickersList from "../../../Components/StickersList";
import NewSticker from "../../../Pages/Collections/Stickers/NewSticker";
import { withFirebase } from "../../../Services/Firebase";
import { withAuthentication, withAuthorization } from "../../../Services/Session";
import { appUseEffect, appUseState } from "../../../Services/Utils/react-hooks";
import styles from "./styles";

const StickersPage = ({ firebase, classes, match }) => {
  const [collectionId] = appUseState(match.params.collectionId);
  const [loadingState, setLoadingState] = appUseState(true);
  const [errorMessage, setErrorMessage] = appUseState(null);
  const [collectionName, setCollectionName] = appUseState(null);
  const [stickersList, setStickersList] = appUseState([]);
  const [addNewSticker, setAddNewSticker] = appUseState(false);
  const [closeNewSticker, setCloseNewSticker] = appUseState(false);

  const handleFilters = (text) => {
    firebase.getStickersByCollection(collectionId).then((stickersDocs) => {
      const list = [];
      stickersDocs.forEach((stickerDoc) => {
        list.push({
          uid: stickerDoc.id,
          ...stickerDoc.data(),
        });
      });

      if (text) {
        setStickersList(
          list.filter((sticker) => sticker.name.toUpperCase().includes(text.toUpperCase()))
        );
      } else {
        setStickersList(list);
      }
    });
  };

  //Tenho de tirar isto, só adicionei para não dar erro :)
  const handleItemClick = () => () => {};
  const handleAddUserSticker = () => () => {};
  const handleDeleteUserSticker = () => () => {};
  const handleDislikeSticker = () => () => {};
  const handleLikeSticker = () => () => {};

  const handleEditClick = (stickerId) => () => {
    setAddNewSticker(false);
    const list = stickersList;

    list.forEach((sticker, index, list) => {
      if (sticker.uid === stickerId) {
        list[index] = { ...sticker, toEdit: true };
      }
    });
    setStickersList([...list]);
  };

  const handleCloseEdit = (stickerId) => () => {
    const list = stickersList;

    list.forEach((sticker, index, list) => {
      if (sticker.uid === stickerId) {
        list[index] = { ...sticker, toEdit: false };
      }
    });
    setStickersList([...list]);
  };

  const [deleteStickerId, setDeleteStickerId] = useState(null);

  const deleteStickerTrue = () => {
    if (deleteStickerId) {
      firebase
        .deleteAdsOfSticker(deleteStickerId)
        .then(() => {
          firebase.deleteSticker(deleteStickerId).then(() => {
            getStickersList();
          });
          setDeleteStickerId(null);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  };

  const deleteStickerFalse = () => {
    setDeleteStickerId(null);
  };

  const handleDelClick = (stickerId) => () => {
    setDeleteStickerId(stickerId);
  };

  const handleAddSticker = () => {
    setAddNewSticker(true);
    setCloseNewSticker(false);
  };

  const handleClose = () => {
    setCloseNewSticker(true);
    setAddNewSticker(false);
  };

  const getStickersList = () => {
    firebase
      .getStickersByCollection(collectionId)
      .then((stickersDocs) => {
        const list = [];
        stickersDocs.forEach((stickerDoc) => {
          list.push({
            uid: stickerDoc.id,
            ...stickerDoc.data(),
          });
        });

        setStickersList(list);
        setLoadingState(false);
      })
      .catch(() => {
        setErrorMessage("Oops, error getting the stickers!");
        setLoadingState(false);
      });
  };

  const getCollectionName = () => {
    firebase
      .getCollectionById(collectionId)
      .then((collection) => setCollectionName(collection.data().name))
      .catch((error) => {
        console.error(error);
        setErrorMessage(false);
      });
  };

  appUseEffect(() => {
    getCollectionName();
    getStickersList();
  }, [firebase]);

  return (
    <div className={classes.stickersListContainer}>
      <ListHeader
        title={collectionName ? `${collectionName} - Stickers` : "Stickers"}
        handleAdd={handleAddSticker}
      />
      <FiltersBar type="stickers" handleFilter={handleFilters} />
      <div className={classes.newStickerContainer}>
        {addNewSticker && !closeNewSticker && (
          <>
            <Typography>New Sticker:</Typography>
            <NewSticker
              collectionId={collectionId}
              handleClose={handleClose}
              updateStickersList={getStickersList}
            />
          </>
        )}
      </div>
      <StickersList
        stickers={stickersList}
        handleItemClick={handleItemClick}
        handleEditClick={handleEditClick}
        handleDelClick={handleDelClick}
        error={errorMessage}
        loading={loadingState}
        collectionId={collectionId}
        handleCloseEdit={handleCloseEdit}
        updateStickersList={getStickersList}
        handleAddUserSticker={handleAddUserSticker}
        handleDeleteUserSticker={handleDeleteUserSticker}
        handleLikeSticker={handleLikeSticker}
        handleDislikeSticker={handleDislikeSticker}
      />
      {deleteStickerId && (
        <ConfirmationModal onTrue={deleteStickerTrue} onFalse={deleteStickerFalse} />
      )}
    </div>
  );
};

StickersPage.propTypes = {
  firebase: any,
  classes: any,
  history: any,
  match: any,
  authUser: any,
};

const condition = (authUser) => !!authUser;

export default withAuthentication(
  withAuthorization(condition)(withRouter(withFirebase(withStyles(styles)(StickersPage))))
);

export const StickersPageTests = StickersPage;
