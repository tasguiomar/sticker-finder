import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import { TextField, Grid, withStyles, Card } from '@material-ui/core';

import { withFirebase } from '../../../../Services/Firebase';
import { any } from 'prop-types';

import Alert from '../../../../Components/Alert';
import styles from './styles';
import ImageUploader from '../../../../Components/ImageUploader';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';

const EditSticker = ({
  sticker,
  firebase,
  classes,
  handleClose,
  updateStickersList,
}) => {
  const [stickerData, setStickerData] = useState({
    ...sticker,
  });
  const [errorMessage, setErrorMessage] = useState(null);

  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setStickerData({ ...stickerData, [name]: value });
  };

  const handleEditSticker = (event) => {
    event.preventDefault();
    const obj = { ...stickerData, toEdit: false };

    firebase
      .updateSticker(sticker.uid, obj)
      .then(() => {
        handleClose();
        updateStickersList();
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const handleErrorClose = () => setErrorMessage(null);

  const handleImageUpload = (url) => {
    setStickerData({ ...stickerData, image: url });
  };

  return (
    <form onSubmit={handleEditSticker}>
      <Card className={classes.stickerItem}>
        <Grid container spacing={2} className={classes.row}>
          <Grid item xs={3} className={classes.gridItem}>
            <div className={classes.imageContainer}>
              <ImageUploader
                handleImageUpload={handleImageUpload}
                image={stickerData.image}
              />
            </div>
          </Grid>
          <Grid item xs={2} className={classes.gridItem}>
            <TextField
              name="name"
              variant="outlined"
              className={classes.textInput}
              placeholder="Sticker Name"
              value={stickerData.name}
              onChange={(event) => handleChange(event)}
            />
          </Grid>
          <Grid item xs={2} className={classes.gridItem}>
            <TextField
              name="code"
              variant="outlined"
              className={classes.textInput}
              placeholder="Sticker Code"
              value={stickerData.code}
              onChange={(event) => handleChange(event)}
            />
          </Grid>
          <Grid item xs={3} className={classes.gridItem}>
            <TextField
              name="description"
              variant="outlined"
              className={classes.textInput}
              placeholder="Description"
              value={stickerData.description}
              onChange={(event) => handleChange(event)}
            />
          </Grid>
          <Grid item xs={2} className={classes.gridItem}>
            <div className={classes.listActions}>
              <Button type="submit">
                <SaveIcon title="Save Sticker" />
              </Button>
              <Button onClick={() => handleClose()}>
                <CancelIcon title="Delete Sticker" />
              </Button>
            </div>
          </Grid>
          <Alert
            type="error"
            message={errorMessage}
            close={true}
            handleClose={handleErrorClose}
          />
        </Grid>
      </Card>
    </form>
  );
};

EditSticker.propTypes = {
  firebase: any,
  classes: any,
  sticker: any,
  handleClose: any,
  updateStickersList: any,
};

export default withFirebase(withStyles(styles)(EditSticker));
