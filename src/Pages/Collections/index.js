// Material UI
import { Grid, Typography, withStyles } from "@material-ui/core";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Alert from "../../Components/Alert";
import CollectionCard from "../../Components/CollectionCard";
import ConfirmationModal from "../../Components/ConfirmationModal";
import FiltersBar from "../../Components/FiltersBar";
import ListHeader from "../../Components/ListHeader";
import * as ROUTES from "../../Constants/routes";
import { withFirebase } from "../../Services/Firebase";
import { withAuthentication, withAuthorization } from "../../Services/Session";
// Styles
import styles from "./styles";

const CollectionsPage = ({ firebase, classes, authUser }) => {
  let history = useHistory();
  const [collections, setCollections] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [userCollections, setUserCollections] = useState([]);
  const [deleteSelectedCollectionId, setDeleteSelectedCollectionId] = useState(
    null
  );
  const userId = authUser && authUser.uid;

  useEffect(() => {
    const array = [];
    setLoading(true);

    userId &&
      firebase.getUserCollections(userId).then((userCollectionsArray) => {
        setUserCollections(
          userCollectionsArray && userCollectionsArray.length > 0
            ? userCollectionsArray
            : []
        );
      });

    firebase
      .getCollections()
      .then(function (collectionsDb) {
        collectionsDb.forEach(function (doc) {
          array.push({ uid: doc.id, ...doc.data() });
        });
        setCollections(array);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
        setErrorMessage("Oops, não foi possivel obter as coleções");
      });
  }, [firebase, authUser, userId]);

  const onDeleteSelectedCollectionTrue = () => {
    if (deleteSelectedCollectionId) {
      firebase
        .deleteCollection(deleteSelectedCollectionId)
        .then(() => {
          setCollections(
            collections.filter(
              (collection) => collection.uid !== deleteSelectedCollectionId
            )
          );
        })
        .catch(() => {
          setErrorMessage("Oops, não foi possivel apagar a coleção");
        });
      setDeleteSelectedCollectionId(null);
    }
  };

  const onDeleteSelectedCollectionFalse = () => {
    setDeleteSelectedCollectionId(null);
  };

  const deleteSelectedCollection = (uidToDelete) => {
    setDeleteSelectedCollectionId(uidToDelete);
  };

  const addCollection = () => {
    history.push(ROUTES.CREATECOLLECTION);
  };

  const editCollection = (collection) => {
    history.push({
      pathname: ROUTES.EDITCOLLECTION,
      state: collection.uid,
    });
  };

  const handleFilters = (category, subCategory, text) => {
    const array = [];
    if (category || subCategory) {
      firebase
        .getCollectionsByCategory(category, subCategory)
        .then(function (collectionsDb) {
          collectionsDb.forEach(function (doc) {
            array.push({ uid: doc.id, ...doc.data() });
          });

          if (text) {
            setCollections(
              array.filter((collection) =>
                collection.name.toUpperCase().includes(text.toUpperCase())
              )
            );
          } else {
            setCollections(array);
          }
        })
        .catch(() => {
          setErrorMessage("Oops, não foi possivel obter as coleções");
        });
    } else {
      firebase
        .getCollections()
        .then(function (collectionsDb) {
          collectionsDb.forEach(function (doc) {
            array.push({ uid: doc.id, ...doc.data() });
          });
          if (text) {
            setCollections(
              array.filter((collection) =>
                collection.name.toUpperCase().includes(text.toUpperCase())
              )
            );
          } else {
            setCollections(array);
          }
        })
        .catch(() => {
          setErrorMessage("Oops, não foi possivel obter as coleções");
        });
    }
  };

  const addCollectionToUser = (event, collection) => {
    event.preventDefault();
    event.stopPropagation();
    const oldCollections = userCollections;

    firebase
      .addUserCollection(userId, collection, userCollections)
      .then(() => setUserCollections([...userCollections, collection.uid]))
      .catch(() => setUserCollections(oldCollections));
  };

  const hasAlreadyCollection = (collection) => {
    const index = userCollections.findIndex(
      (collectionId) => collectionId === collection.uid
    );

    return index === -1 ? true : false;
  };

  function ListCollections() {
    return collections.map((collection) => {
      const handleClick = () =>
        history.push(ROUTES.STICKERS.replace(":collectionId", collection.uid));
      return (
        <Grid key={collection.uid} item xs={12} md={6} lg={3}>
          <CollectionCard
            handleClick={handleClick}
            collection={collection}
            deleteCollection={deleteSelectedCollection}
            editCollection={editCollection}
            addUserCollection={(event) =>
              addCollectionToUser(event, collection)
            }
            userCanAdd={hasAlreadyCollection(collection)}
          />
        </Grid>
      );
    });
  }

  return (
    <div className="main-container">
      <ListHeader title="Collections" handleAdd={addCollection} />
      <FiltersBar type="collection" handleFilter={handleFilters} />
      {!isLoading && (
        <>
          <h4>
            Tip: In here you can see our available collections, you can add them
            to your profile if you want to keep updated about the collection
            ads!
          </h4>
          <Grid container spacing={4} className={classes.collectionsList}>
            {collections.length > 0 ? (
              ListCollections()
            ) : (
              <div className="empty-message">
                <Typography>No collections available!</Typography>
              </div>
            )}
          </Grid>
        </>
      )}
      <Alert
        type="error"
        message={errorMessage}
        close={true}
        handleClose={() => setErrorMessage(null)}
      />

      {deleteSelectedCollectionId && (
        <ConfirmationModal
          onTrue={onDeleteSelectedCollectionTrue}
          onFalse={onDeleteSelectedCollectionFalse}
        />
      )}
    </div>
  );
};

CollectionsPage.propTypes = {
  firebase: any,
  classes: any,
  authUser: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withAuthentication(withFirebase(withStyles(styles)(CollectionsPage)))
);
