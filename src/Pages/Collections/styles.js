import { createStyles } from '@material-ui/core';
const styles = () =>
  createStyles({
    collectionsList: {
      padding: '16px',
    },
    emptyCollection: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      height: 200,
      justifyContent: 'center',
    },
  });

export default styles;
