// Material
import { withStyles } from "@material-ui/core";
// React
import { any } from "prop-types";
import React from "react";
// Local
import StickersList from "../../../Components/StickersList";
import { withFirebase } from "../../../Services/Firebase";
import {
  withAuthentication,
  withAuthorization,
} from "../../../Services/Session";
import { appUseEffect, appUseState } from "../../../Services/Utils/react-hooks";
import styles from "./styles";

const SelectStickersPage = ({ firebase, classes, match, authUser }) => {
  const [collectionId] = appUseState(match.params.collectionId);
  const [loadingState, setLoadingState] = appUseState(true);
  const [isUserStickersPage, setIsUserStickersPage] = appUseState(true);
  const [errorMessage, setErrorMessage] = appUseState(null);
  const [stickersList, setStickersList] = appUseState([]);
  const [userStickersList, setUserStickersList] = appUseState([]);

  const [collectionInformation, setCollectionInformation] = React.useState(
    null
  );

  const userId = authUser && authUser.uid;

  appUseEffect(() => {
    const array = [];

    setLoadingState(true);
    setIsUserStickersPage(true);
    getStickersList();

    collectionId &&
      firebase
        .getCollectionById(collectionId)
        .then((response) => setCollectionInformation(response.data()));

    userId &&
      firebase.getUserStickers(userId).then(function (collectionsDb) {
        collectionsDb &&
          collectionsDb.length > 0 &&
          collectionsDb.forEach(function (sticker) {
            array.push(sticker);
          });
        setUserStickersList(array);
        setLoadingState(false);
      });
  }, [firebase, authUser, userId]);

  //Tirar depois, só para não dar erro :)
  const handleItemClick = () => () => {};
  const handleEditClick = () => () => {};
  const handleCloseEdit = () => () => {};
  const handleDelClick = () => () => {};

  const hasAlreadySticker = (stickerId) => {
    const index = userStickersList.findIndex(
      (sticker) => sticker === stickerId
    );
    return index === -1 ? true : false;
  };

  const handleAddUserSticker = (stickerId) => () => {
    const oldStickers = userStickersList;

    firebase
      .addUserSticker(userId, stickerId, userStickersList)
      .then(() => {
        setUserStickersList([...userStickersList, stickerId]);
      })
      .catch(() => {
        setUserStickersList(oldStickers);
      });
  };

  const handleLikeSticker = (stickerId) => () => {
    if (authUser) {
      firebase.favSticker(authUser.uid, stickerId).catch((error) => {
        setErrorMessage("Oops, error liking a sticker!");
        console.error(error);
      });
    }
  };

  const handleDislikeSticker = (favId) => {
    firebase.removeFavSticker(favId).catch((error) => {
      setErrorMessage("Oops, error unliking a sticker!");
      console.error(error);
    });
  };

  const handleDeleteUserSticker = (stickerId) => () => {
    const oldStickers = userStickersList;
    const newStickers = [];

    userStickersList.forEach((sticker) => {
      if (sticker !== stickerId) {
        newStickers.push(sticker);
      }
    });

    firebase
      .deleteUserSticker(userId, newStickers)
      .then(() => {
        setUserStickersList(newStickers);
      })
      .catch(() => {
        setUserStickersList(oldStickers);
      });
  };

  const getStickersList = () => {
    firebase
      .getStickersByCollection(collectionId)
      .then((stickersDocs) => {
        const list = [];
        stickersDocs.forEach((stickerDoc) => {
          list.push({
            uid: stickerDoc.id,
            ...stickerDoc.data(),
          });
        });
        setStickersList(list);
        setLoadingState(false);
      })
      .catch(() => {
        setErrorMessage("Oops, error getting the stickers!");
        setLoadingState(false);
      });
  };

  return (
    <div className={classes.stickersListContainer}>
      <h1 className={classes.title}>
        Manage your {collectionInformation && collectionInformation.name}{" "}
        stickers{" "}
      </h1>
      <h4>
        Tip: In here you can add or remove stickers from you collection so you
        can keep track of everything!
      </h4>
      <h4>
        You can also add and remove stickers from your wish list. If a sticker
        in your wish list gets an add, you will be notified
      </h4>
      <StickersList
        stickers={stickersList}
        handleItemClick={handleItemClick}
        handleEditClick={handleEditClick}
        handleDelClick={handleDelClick}
        error={errorMessage}
        loading={loadingState}
        collectionId={collectionId}
        handleCloseEdit={handleCloseEdit}
        updateStickersList={getStickersList}
        handleAddUserSticker={handleAddUserSticker}
        handleDeleteUserSticker={handleDeleteUserSticker}
        handleLikeSticker={handleLikeSticker}
        handleDislikeSticker={handleDislikeSticker}
        isUserStickersPage={isUserStickersPage}
        addButtonOn={hasAlreadySticker}
      />
    </div>
  );
};

SelectStickersPage.propTypes = {
  firebase: any,
  classes: any,
  history: any,
  match: any,
  authUser: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withAuthentication(withFirebase(withStyles(styles)(SelectStickersPage)))
);
