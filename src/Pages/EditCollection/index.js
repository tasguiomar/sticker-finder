/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react';

import Button from '@material-ui/core/Button';
import { TextField, Grid, withStyles } from '@material-ui/core';
import { useHistory } from 'react-router';
import * as ROUTES from '../../Constants/routes';

import { withFirebase } from '../../Services/Firebase';
import { any } from 'prop-types';

import Alert from '../../Components/Alert';
import PageWrapper from '../../Components/PageWrapper';
import styles from '../Login/styles';
import ImageUploader from '../../Components/ImageUploader';
import { withAuthorization } from '../../Services/Session';

import Select from 'react-select';

const EditCollection = ({ firebase, classes }) => {
  let history = useHistory();

  const [collectionCategories, setCollectionCategories] = useState([]);
  const [collectionSubCategories, setCollectionSubCategories] = useState([]);

  const [selectedCategory, selectCategory] = useState('');
  const [selectedSubCategories, selectSubCategories] = useState([]);

  const [collectionData, setCollectionData] = useState({
    name: '',
    description_long: '',
    description_short: '',
    image: '',
  });
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    const array = [];
    firebase
      .getCollectionCategories()
      .then(function (collectionsDb) {
        collectionsDb.forEach(function (doc) {
          array.push({ uid: doc.id, ...doc.data() });
        });

        setCollectionCategories(array);

        firebase
          .getCollectionById(history.location.state)
          .then(function (collection) {
            setCollectionData(collection.data());
            selectCategory(collection.data().collectionId);
            selectSubCategories(collection.data().subCategoriesId);

            if (collection.data().subCategoriesId.length > 0) {
              const subCategories = [];

              collection.data().subCategoriesId.forEach((subCategory) =>
                subCategories.push({
                  value: subCategory,
                  label: subCategory,
                })
              );
              setCollectionSubCategories(subCategories);
            }
          });
      })
      .catch(() => setLoadingState(false));
  }, [firebase, history.location.state]);

  const handleUpdateCollection = (event) => {
    event.preventDefault();
    firebase
      .updateCollection(
        {
          ...collectionData,
          collectionId: selectedCategory,
          subCategoriesId: selectedSubCategories,
        },
        history.location.state
      )
      .then(function () {
        history.push(ROUTES.COLLECTIONS);
      })
      .catch((error) => {
        setErrorMessage(`${error}`);
      });
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setCollectionData({ ...collectionData, [name]: value });
  };

  const handleErrorClose = () => setErrorMessage(null);

  const handleImageUpload = (url) => {
    setCollectionData({ ...collectionData, image: url });
  };

  const handleChangeSelect = (event, name, type) => {
    if (name === 'category') {
      const { value } = event;

      selectCategory(value);
      selectSubCategories([]);
      !value && setCollectionSubCategories([]);

      collectionCategories.forEach((collection) => {
        if (collection.uid === value) {
          setCollectionSubCategories(collection.subCategories);
        }
      });
    }
    if (name === 'subCategory') {
      if (type.action === 'select-option') {
        const array = [];
        array.push(type.option.value);
        selectedSubCategories.length > 0 &&
          array.push(...selectedSubCategories);
        selectSubCategories(array);
      } else if (type.action === 'remove-value') {
        const array = [];
        selectedSubCategories.length > 0 &&
          selectedSubCategories.forEach((subCategory) => {
            if (subCategory !== type.removedValue.value) {
              array.push(subCategory);
            }
          });
        selectSubCategories(array);
      } else if (type.action === 'clear') {
        selectSubCategories([]);
      }
    }
  };

  const getCollectionCategoriesOptions = (defaultOption) => {
    const array = [];
    if (defaultOption) {
      collectionCategories.forEach((collection) => {
        collection.uid === defaultOption &&
          array.push({ value: collection.uid, label: collection.name });
      });
    } else {
      collectionCategories.forEach((collection) => {
        array.push({ value: collection.uid, label: collection.name });
      });
    }

    return array;
  };

  const getCollectionSubCategoriesOptions = (defaultOptions) => {
    const array = [];
    if (defaultOptions) {
      defaultOptions.length > 0 &&
        collectionSubCategories.forEach((collection) => {
          defaultOptions.forEach((optionFromArray) => {
            collection.value
              ? collection.value === optionFromArray &&
                array.push({ value: optionFromArray, label: optionFromArray })
              : collection.id === optionFromArray &&
                array.push({ value: optionFromArray, label: optionFromArray });
          });
        });
    } else {
      collectionSubCategories.forEach((collection) => {
        collection.id
          ? array.push({ value: collection.id, label: collection.id })
          : array.push(collection);
      });
    }

    return array;
  };

  return (
    <PageWrapper>
      <form className={classes.card} onSubmit={handleUpdateCollection}>
        <Grid item container xs={12} spacing={2} style={{ height: '100%' }}>
          <Grid item xs={6} className={classes.gridItem}>
            <ImageUploader
              handleImageUpload={handleImageUpload}
              image={collectionData.image}
            />
          </Grid>
          <Grid xs={6} style={{ flexDirection: 'column', margin: 'auto' }}>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="name"
                variant="outlined"
                className={classes.textInputCollection}
                placeholder="Collection Name"
                value={collectionData.name}
                onChange={(event) => handleChange(event)}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="description_long"
                multiline
                rows={4}
                rowsMax={6}
                variant="outlined"
                className={classes.textInputCollection}
                placeholder="Long Description"
                value={collectionData.description_long}
                onChange={(event) => handleChange(event)}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="description_short"
                variant="outlined"
                multiline
                rows={4}
                rowsMax={6}
                className={classes.textInputCollection}
                placeholder="Short Description"
                value={collectionData.description_short}
                onChange={(event) => handleChange(event)}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <Select
                value={getCollectionCategoriesOptions(selectedCategory)}
                onChange={(event) => handleChangeSelect(event, 'category')}
                className={classes.textInputCollection}
                options={getCollectionCategoriesOptions()}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <Select
                onChange={(event, type) => {
                  handleChangeSelect(event, 'subCategory', type);
                }}
                value={getCollectionSubCategoriesOptions(selectedSubCategories)}
                className={classes.textInputCollection}
                isMulti
                options={getCollectionSubCategoriesOptions()}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <Button
                className={classes.confirmButton}
                variant="outlined"
                type="submit"
              >
                Edit Collection{' '}
              </Button>{' '}
            </Grid>
          </Grid>
        </Grid>
        <Alert
          type="error"
          message={errorMessage}
          close={true}
          handleClose={handleErrorClose}
        />{' '}
      </form>{' '}
    </PageWrapper>
  );
};

EditCollection.propTypes = {
  firebase: any,
  classes: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withFirebase(withStyles(styles)(EditCollection))
);
