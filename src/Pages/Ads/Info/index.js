// Material

// React
import {
  Button,
  FormControl,
  Grid,
  TextField,
  Typography,
  withStyles,
} from "@material-ui/core";
import emailjs from "emailjs-com";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import AlertComponent from "../../../Components/Alert";
import LoadingComponent from "../../../Components/Loading";
import * as ROUTES from "../../../Constants/routes";
import { withFirebase } from "../../../Services/Firebase";
import {
  withAuthentication,
  withAuthorization,
} from "../../../Services/Session";
import styles from "./styles";

const AdsInfo = ({ firebase, classes, authUser }) => {
  let history = useHistory();
  const [adData, setAdData] = useState(null);
  const [errorMessage, setErrorMessage] = useState("");
  const [collections, setCollections] = useState([]);
  const [stickers, setStickers] = useState([]);

  const [contactReason, setContactReason] = useState([]);

  const [userInfo, setUserInfo] = useState(null);
  const [announcerInfo, setAnnouncerInfo] = useState(null);

  const [loading, setLoading] = useState(false);

  const adId = history.location.state;
  const userId = authUser && authUser.uid;

  useEffect(() => {
    userId && firebase.getUser(userId).then((user) => setUserInfo(user));

    if (adId) {
      firebase.getAdInfoById(adId).then((response) => {
        userId &&
          firebase
            .getUser(response.userId)
            .then((user) => setAnnouncerInfo(user));
        setAdData(response);
        firebase
          .getStickersByCollection(response.collection)
          .then(function (collectionsDb) {
            const stickersArray = [];
            collectionsDb.forEach(function (doc) {
              stickersArray.push({ uid: doc.id, ...doc.data() });
            });

            setStickers(stickersArray);
          });
      });

      firebase
        .getCollections()
        .then(function (collectionsDb) {
          const collectionsArray = [];
          collectionsDb.forEach(function (doc) {
            collectionsArray.push({ uid: doc.id, ...doc.data() });
          });
          setCollections(collectionsArray);
        })
        .catch(() => {
          setCollections([]);
        });
    }
  }, [firebase, adId, userId]);

  const getCollectionName = (uid) => {
    let name = "";
    collections.forEach((collection) => {
      if (collection.uid === uid) {
        name = collection.name;
      }
    });
    return name;
  };

  const getStickerName = (uid) => {
    let name = "";
    stickers.forEach((sticker) => {
      if (sticker.uid === uid) {
        name = sticker.name;
      }
    });
    return name;
  };

  const handleChange = (e) => {
    setContactReason(e.target.value);
  };

  const sendEmail = (e) => {
    e.preventDefault();
    setLoading(true);
    emailjs
      .send(
        "gmail",
        "TemplateAd",
        {
          senderEmail: userInfo.email,
          fromName: "Sticker Finder",
          toName: announcerInfo.name,
          receiverEmail: announcerInfo.email,
          messageHtml: contactReason,
        },
        "user_X4onLRIkqBpN8hFwnoFDu"
      )
      .then(
        () => {
          window.alert("Your email was sent!");
          history.goBack();
        },
        () => {
          window.alert("Oops we could not send your email!");
        }
      )
      .finally(() => setLoading(false));
  };

  const Userprofile = (announcerInfo) => {
    history.push({
      pathname: ROUTES.OTHERUSERSPROFILEDETAILS,
      state: announcerInfo.id,
    });
  };

  return loading ? (
    <LoadingComponent />
  ) : adData ? (
    <div className={classes.formContainer}>
      <div className={classes.formWrapper}>
        <h1>{`AD ${adData.code}`}</h1>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <img src={adData.image} alt="Ad" style={{ maxWidth: "100%" }} />
          </Grid>
          <Grid item container xs={12} md={8}>
            <Grid item xs={6}>
              <Typography variant="h6"> Collection:</Typography>
              <Typography> {getCollectionName(adData.collection)}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6"> Name:</Typography>
              <Typography> {adData.name}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6"> Sticker:</Typography>
              <Typography>{getStickerName(adData.sticker)}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6"> Add Created By:</Typography>
              <Typography>{announcerInfo ? announcerInfo.name : ""}</Typography>
            </Grid>

            <Grid item xs={6}>
              <Typography variant="h6"> Description:</Typography>
              <Typography>{adData.description}</Typography>
            </Grid>
            <Grid item xs={6}>
              <div style={{ marginTop: 36 }} />
              <Button
                className={classes.addButton}
                variant="outlined"
                type="submit"
                onClick={() => Userprofile(announcerInfo)}
              >
                View user details
              </Button>
            </Grid>
          </Grid>
          <form style={{ width: "100%" }} onSubmit={sendEmail}>
            <Grid item xs={12} style={{ paddingTop: 50 }}>
              <Typography variant="h6"> Contact Another User:</Typography>

              <FormControl className={classes.formControl}>
                <TextField
                  name="contactReason"
                  placeholder="Write your message to the announcer here!"
                  onChange={handleChange}
                  variant="outlined"
                  multiline
                  rows={5}
                  rowsMax={5}
                  maxLength={300}
                  className={classes.textInput}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} className={classes.confirmButtonContainer}>
              <Button
                className={classes.confirmButton}
                variant="outlined"
                type="submit"
              >
                Submit
              </Button>
            </Grid>
          </form>
        </Grid>
        <AlertComponent
          type="error"
          message={errorMessage}
          close={true}
          handleClose={() => setErrorMessage("")}
        />
      </div>
    </div>
  ) : (
    <LoadingComponent />
  );
};

AdsInfo.propTypes = {
  firebase: any,
  classes: any,
  match: any,
  authUser: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withAuthentication(withFirebase(withStyles(styles)(AdsInfo)))
);
