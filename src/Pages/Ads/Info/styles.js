import { createStyles } from '@material-ui/core';
const blue = '#7EA6E0';
const styles = (theme) =>
  createStyles({
    formContainer: {
      background: '#CCE5FF',
      height: '100%',
      padding: '2px',
      position: 'relative',
      overflow: 'auto',
    },
    addButton: {
      color: blue,
    },
    formWrapper: {
      background: '#FFFFFF',
      borderRadius: '10px',
      boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
      maxWidth: '1200px',
      width: '80%',
      padding: '5px 40px 40px',
      margin: '40px auto',
    },
    form: {
      padding: '5px 0',
    },
    formControl: {
      display: 'flex',
      padding: '10px 20px 5px',
      position: 'relative',
      width: '100%',
      '& > div': {
        flexGrow: 1,
      },
      '& input': {
        padding: '14px 10px',
      },
      '& label': {
        position: 'absolute',
        left: 'auto',
        top: '50%',
        transform: 'translateY(-50%)',
        color: '#9e9e9e',
        padding: '4px 10px 0 10px',
      },
    },
    selects: {
      display: 'flex',
      flexWrap: 'wrap',
      maxWidth: '600px',
      '& > div': {
        minWidth: '250px',
        flexGrow: 1,
        flexBasis: 0,
      },
    },
    select: {
      padding: '16px 10px',
    },
    imageContainer: {
      alignItems: 'center',
      background: '#CCCCCC',
      borderRadius: '4px',
      cursor: 'pointer',
      display: 'flex',
      justifyContent: 'center',
      width: '100%',
      overflow: 'hidden',
      position: 'relative',
      height: '100%',
      margin: 'auto',
      '& > img': {
        margin: 'auto',
        width: '102%',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
      },
    },
    confirmButtonContainer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: '10px 20px 5px',
    },
    confirmButton: {
      width: '100%',
      display: 'flex',
      alignSelf: 'flex-end',
      maxWidth: 200,
      '&:not(:disabled)': {
        borderRadius: 10,
        backgroundColor: '#0085FC',
        color: 'white',
      },
    },
    textInput: {
      borderRadius: 10,
      width: '100%',
      '& .MuiOutlinedInput-root': {
        borderRadius: 10,
      },
    },
    [theme.breakpoints.down('sm')]: {
      imageContainer: {
        maxWidth: '50%',
        minHeight: '200px',
      },
    },
    // Add new
  });

export default styles;
