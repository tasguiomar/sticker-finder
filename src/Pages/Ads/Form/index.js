// Material
import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  withStyles,
} from "@material-ui/core";
// React
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";
// Local
import AlertComponent from "../../../Components/Alert/index.js";
import * as ROUTES from "../../../Constants/routes";
import { withFirebase } from "../../../Services/Firebase/index.js";
import { withAuthentication, withAuthorization } from "../../../Services/Session/index.js";
import styles from "./styles.js";

const AdsImageUploader = ({ classes, image, onImageSelect }) => {
  const [fileReader] = useState(new FileReader());
  const [tempImage, setTempImage] = useState(image);
  const [inputFile, setInputFile] = useState();

  const handleImageClick = () => {
    inputFile.click();
  };

  useEffect(() => {
    if (tempImage.image) {
      fileReader.addEventListener("load", (event) => {
        if (tempImage) {
          onImageSelect({ image: tempImage.image, src: event.target.result });
        }
      });

      fileReader.readAsDataURL(tempImage.image);
    }
  }, [fileReader, tempImage, onImageSelect]);

  const handleImageSelect = () => {
    if (inputFile.files.length) {
      setTempImage({ image: inputFile.files[0], src: null });
    }
  };

  return (
    <div onClick={handleImageClick} className={classes.imageContainer}>
      <input
        ref={setInputFile}
        type="file"
        className="d-none"
        onChange={handleImageSelect}
        accept="image/png, image/jpeg, image/jpg"
      />
      <img src={image.src} alt="Ad" />
    </div>
  );
};

const AdsForm = ({ firebase, classes, history, match, authUser }) => {
  const defaultImage =
    "https://firebasestorage.googleapis.com/v0/b/sticker-finder.appspot.com/o/images%2Fplaceholder.jpg?alt=media&token=3221ad04-48bd-4c86-a2df-932a414c6fcb";
  const [adData, setAdData] = useState({
    code: "",
    name: "",
    description: "",
    collection: "",
    sticker: "",
  });
  const [errorMessage, setErrorMessage] = useState(null);
  const [collections, setCollections] = useState([]);
  const [stickers, setStickers] = useState([]);
  const [image, setImage] = useState({ image: null, src: defaultImage });
  const [adId, setAdId] = useState(match.params.adId);

  const isValid =
    adData.code &&
    adData.name &&
    adData.description &&
    adData.collection &&
    adData.sticker &&
    image !== null &&
    image !== defaultImage &&
    !errorMessage;

  const handleChange = (e) => {
    setAdData({
      ...adData,
      [e.target.name]: e.target.value,
    });
  };

  const handleCollectionChange = (e) => {
    setAdData({
      ...adData,
      collection: e.target.value,
      sticker: "",
    });
  };

  useEffect(() => {
    firebase
      .getCollections()
      .then(function (collectionsDb) {
        const collectionsArray = [];
        collectionsDb.forEach(function (doc) {
          collectionsArray.push({ uid: doc.id, ...doc.data() });
        });

        setCollections(collectionsArray);
      })
      .catch(() => {
        setCollections([]);
      });
  }, [firebase]);

  useEffect(() => {
    if (adId) {
      firebase
        .getAdById(adId)
        .then(function (adDoc) {
          setAdData({
            ...adDoc.data(),
          });
          setImage({ image: null, src: adDoc.data().image });
        })
        .catch((error) => {
          console.error(error);
          setAdId(null);
          setErrorMessage(
            "Error getting data from ad with that id! You've been redirected to Create Ad Page."
          );
          history.push(ROUTES.ADSFORM.replace(":adId?", ""));
        });
    }
  }, [firebase, adId, history]);

  useEffect(() => {
    const collectionId = adData.collection;
    if (collectionId) {
      firebase.getStickersByCollection(collectionId).then(function (collectionsDb) {
        const stickersArray = [];
        collectionsDb.forEach(function (doc) {
          stickersArray.push({ uid: doc.id, ...doc.data() });
        });

        setStickers(stickersArray);
      });
    }
  }, [firebase, adData.collection]);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!image || !image.image) {
      handleImageUploadSuccess(image.src);
      return;
    }
    firebase.uploadImage(image.image, handleImageUploadSuccess, handleImageUploadError);
  };

  const handleImageUploadSuccess = (imageUrl) => {
    const submitAdModel = {
      ...adData,
      userId: authUser.uid,
      image: imageUrl,
    };
    let submitPromise = null;

    if (adId) {
      submitPromise = firebase.editAd(adId, submitAdModel);
    } else {
      submitPromise = firebase.addAd(submitAdModel);
    }

    submitPromise
      .then(() => {
        history.push(ROUTES.ADS);
      })
      .catch((error) => {
        console.error(error);
        handleImageUploadError();
      });
  };

  const handleImageUploadError = () => {
    setErrorMessage("Error submitting new ad! Please try again later...");
  };

  const handleCloseErrorMessage = () => {
    setErrorMessage(null);
  };

  const title = adId ? "Edit ad" : "Add New Ad";

  return (
    <div className={classes.formContainer}>
      <div className={classes.formWrapper}>
        <h1>{title}</h1>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={4}>
              <AdsImageUploader classes={classes} image={image} onImageSelect={setImage} />
            </Grid>
            <Grid item xs={12} md={8}>
              <div className={["w-100", classes.selects].join(" ")}>
                <div className={classes.formControl}>
                  <InputLabel
                    id="collection-label"
                    className={["select", adData.collection ? "d-none" : ""].join(" ")}
                  >
                    Collection
                  </InputLabel>
                  <Select
                    labelId="collection-label"
                    id="collection"
                    name="collection"
                    className="select"
                    variant="outlined"
                    value={adData.collection}
                    onChange={handleCollectionChange}
                    displayEmpty
                  >
                    {collections.map((collection) => (
                      <MenuItem key={collection.uid} value={collection.uid}>
                        {collection.name}
                      </MenuItem>
                    ))}
                  </Select>
                </div>
                <FormControl className={classes.formControl}>
                  <InputLabel
                    id="sticker-label"
                    className={["select", adData.sticker ? "d-none" : ""].join(" ")}
                  >
                    Sticker
                  </InputLabel>
                  <Select
                    labelId="sticker-label"
                    id="sticker"
                    name="sticker"
                    className="select"
                    variant="outlined"
                    value={adData.sticker}
                    onChange={handleChange}
                    displayEmpty
                    disabled={!adData.collection}
                  >
                    {stickers.map((sticker) => (
                      <MenuItem key={sticker.uid} value={sticker.uid}>
                        {sticker.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
              <div className={classes.formControl}>
                <TextField
                  name="code"
                  variant="outlined"
                  className={classes.textInput}
                  placeholder="Code"
                  value={adData.code}
                  onChange={handleChange}
                />
              </div>
              <div className={classes.formControl}>
                <TextField
                  name="name"
                  variant="outlined"
                  className={classes.textInput}
                  placeholder="Name"
                  value={adData.name}
                  onChange={handleChange}
                />
              </div>
              <div className={classes.formControl}>
                <TextField
                  name="description"
                  variant="outlined"
                  multiline
                  rows={4}
                  rowsMax={6}
                  className={classes.textInput}
                  placeholder="Description"
                  value={adData.description}
                  onChange={handleChange}
                />
              </div>
            </Grid>
            <Grid item xs={12} md={4} className="m-0 p-0"></Grid>
            <Grid item xs={12} md={8}>
              <div className={[classes.formControl, "py-0"].join(" ")}>
                <Button
                  className={classes.confirmButton}
                  variant="outlined"
                  type="submit"
                  disabled={!isValid}
                >
                  Submit
                </Button>
              </div>
            </Grid>
          </Grid>
          <AlertComponent
            type="error"
            message={errorMessage}
            close={true}
            handleClose={handleCloseErrorMessage}
          />
        </form>
      </div>
    </div>
  );
};

AdsForm.propTypes = {
  firebase: any,
  classes: any,
  history: any,
  match: any,
  authUser: any,
};

AdsImageUploader.propTypes = {
  classes: any,
  image: any,
  onImageSelect: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withAuthentication(withFirebase(withRouter(withStyles(styles)(AdsForm))))
);
