/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react';

import Button from '@material-ui/core/Button';
import { TextField, Grid, withStyles } from '@material-ui/core';
import { useHistory } from 'react-router';
import * as ROUTES from '../../Constants/routes';

import { withFirebase } from '../../Services/Firebase';
import { any } from 'prop-types';

import { withAuthorization } from '../../Services/Session';

import Alert from '../../Components/Alert';
import PageWrapper from '../../Components/PageWrapper';
import styles from '../Login/styles';
import ImageUploader from '../../Components/ImageUploader';

import Select from 'react-select';

const CreateCollection = ({ firebase, classes }) => {
  let history = useHistory();
  const [collectionData, setCollectionData] = useState({
    nome: '',
    description_long: '',
    description_short: '',
    image: 'https://www.laststicker.com/i/album/3236.jpg',
  });

  const [errorMessage, setErrorMessage] = useState(null);
  const [imgUrl, setImageUrl] = useState(null);

  const [collectionCategories, setCollectionCategories] = useState([]);
  const [collectionSubCategories, setCollectionSubCategories] = useState([]);

  const [selectedCategory, selectCategory] = useState('');
  const [selectedSubCategories, selectSubCategories] = useState([]);

  const handleCreateCollection = (event) => {
    event.preventDefault();
    firebase
      .createCollection({
        ...collectionData,
        image: imgUrl,
        collectionId: selectedCategory,
        subCategoriesId: selectedSubCategories.map(
          (subCategory) => subCategory.value
        ),
      })
      .then(function () {
        history.push(ROUTES.COLLECTIONS);
      })
      .catch((error) => {
        setErrorMessage(`${error}`);
      });
  };

  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setCollectionData({ ...collectionData, [name]: value });
  };

  const handleErrorClose = () => setErrorMessage(null);

  const handleImageUpload = (url) => {
    setImageUrl(url);
  };

  useEffect(() => {
    const array = [];
    firebase.getCollectionCategories().then(function (collectionsDb) {
      collectionsDb.forEach(function (doc) {
        array.push({ uid: doc.id, ...doc.data() });
      });

      setCollectionCategories(array);
    });
  }, [firebase]);

  const handleChangeSelect = (event, name) => {
    const { value } = event;

    if (name === 'category') {
      selectCategory(value);
      !value && selectSubCategories('');

      collectionCategories.forEach((collection) => {
        if (collection.uid === value) {
          setCollectionSubCategories(collection.subCategories);
        }
      });
    }
    if (name === 'subCategory') {
      selectSubCategories(event);
    }
  };

  return (
    <PageWrapper>
      <form className={classes.card} onSubmit={handleCreateCollection}>
        <Grid item container xs={12} spacing={2} style={{ height: '100%' }}>
          <Grid item xs={6} className={classes.gridItem}>
            <ImageUploader handleImageUpload={handleImageUpload} />
          </Grid>
          <Grid xs={6} style={{ flexDirection: 'column', margin: 'auto' }}>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="name"
                variant="outlined"
                className={classes.textInputCollection}
                placeholder="Collection Name"
                value={collectionData.name}
                onChange={(event) => handleChange(event)}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="description_long"
                multiline
                rows={4}
                rowsMax={6}
                variant="outlined"
                className={classes.textInputCollection}
                placeholder="Long Description"
                value={collectionData.description_long}
                onChange={(event) => handleChange(event)}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <TextField
                name="description_short"
                variant="outlined"
                multiline
                rows={4}
                rowsMax={6}
                className={classes.textInputCollection}
                placeholder="Short Description"
                value={collectionData.description_short}
                onChange={(event) => handleChange(event)}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <Select
                onChange={(event) => handleChangeSelect(event, 'category')}
                className={classes.textInputCollection}
                options={collectionCategories.map((collection) => {
                  return { value: collection.uid, label: collection.name };
                })}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <Select
                onChange={(event) => handleChangeSelect(event, 'subCategory')}
                closeMenuOnSelect={false}
                className={classes.textInputCollection}
                isMulti
                options={collectionSubCategories.map((collection) => {
                  return { value: collection.id, label: collection.name };
                })}
              />
            </Grid>
            <Grid item xs={12} className={classes.gridItem}>
              <Button
                className={classes.confirmButtonCollection}
                variant="outlined"
                type="submit"
              >
                Create Collection
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Alert
          type="error"
          message={errorMessage}
          close={true}
          handleClose={handleErrorClose}
        />
      </form>
    </PageWrapper>
  );
};

CreateCollection.propTypes = {
  firebase: any,
  classes: any,
};

const condition = (authUser) => !!authUser;

export default withAuthorization(condition)(
  withFirebase(withStyles(styles)(CreateCollection))
);
