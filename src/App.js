// Types
import { Typography } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
// Navigation
import { BrowserRouter as Router, Route } from "react-router-dom";
// Styles
import "./App.css";
import BellNotifications from "./Components/BellNotifications";
// Components
import Navigation from "./Components/Navigation";
import * as ROUTES from "./Constants/routes";
import AdminPage from "./Pages/Admin";
import AdForm from "./Pages/Ads/Form";
import AdsInfo from "./Pages/Ads/Info";
import AdsPage from "./Pages/AdsList";
// Page Components
import CollectionsPage from "./Pages/Collections";
import SelectStickersPage from "./Pages/Collections/SelectStickers";
import StickersPage from "./Pages/Collections/Stickers";
import WishListPage from "./Pages/Collections/Stickers/WishList";
import CreateCollection from "./Pages/CreateCollection";
import EditCollection from "./Pages/EditCollection";
import ForgotPassword from "./Pages/ForgotPassword";
import HomePage from "./Pages/Home";
import LoginPage from "./Pages/Login";
import OtherUsersProfile from "./Pages/OtherUsersProfile";
import OtherUsersProfileDetails from "./Pages/OtherUsersProfile/details";
import RegisterPage from "./Pages/Register";
import UserCollectionsPage from "./Pages/UserCollections";
// Context
import { withAuthentication } from "./Services/Session";

const getCssDisplay = (id) => {
  const elem = document.getElementById(id);
  if (elem) {
    return getComputedStyle(document.getElementById(id), null).display;
  }
  return "initial";
};

const App = ({ authUser }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [navContainerDisplay, setNavContainerDisplay] = useState("block");

  const handleMenuOpenClick = () => setMenuOpen(!menuOpen);

  useEffect(() => {
    if (navContainerDisplay === "flex") {
      // flex is the display for navbar continer when layout is desktop
      setMenuOpen(false);
    }
    const subs = window.addEventListener("resize", () => {
      setNavContainerDisplay(getCssDisplay("navigation-container"));
    });

    return () => {
      window.removeEventListener("resize", subs);
    };
  }, [navContainerDisplay]);

  return (
    <Router>
      <div className="app-container">
        <div className="notifications-bell-container">
          <BellNotifications />
        </div>
        <div className="navbar-container">
          <Typography>Sticker Finder</Typography>
          <div className="menu-icon-container" onClick={handleMenuOpenClick}>
            <MenuIcon />
          </div>
        </div>
        {authUser ? (
          <div
            id="navigation-container"
            className={["navigation-container", menuOpen ? " open" : ""].join("")}
            onClick={handleMenuOpenClick}
          >
            <Navigation />
          </div>
        ) : null}
        <div className="pages-container">
          <div className={menuOpen ? "overlay" : ""} onClick={handleMenuOpenClick}></div>
          <Route path={ROUTES.REGISTER} component={RegisterPage} />
          <Route exact path={ROUTES.LOGIN} component={LoginPage} />
          <Route exact path={ROUTES.FORGOT_PW} component={ForgotPassword} />
          <Route exact path={ROUTES.STICKERS} component={StickersPage} />
          <Route exact path={ROUTES.COLLECTIONS} component={CollectionsPage} />
          <Route exact path={ROUTES.OTHERUSERSPROFILE} component={OtherUsersProfile} />
          <Route
            exact
            path={ROUTES.OTHERUSERSPROFILEDETAILS}
            component={OtherUsersProfileDetails}
          />
          <Route path={ROUTES.CREATECOLLECTION} component={CreateCollection} />
          <Route path={ROUTES.EDITCOLLECTION} component={EditCollection} />
          <Route exact path={ROUTES.ADS} component={AdsPage} />
          <Route exact path={ROUTES.ADSFORM} component={AdForm} />
          <Route path={ROUTES.ADMIN} component={AdminPage} />
          <Route exact path={ROUTES.USERCOLLECTIONS} component={UserCollectionsPage} />
          <Route exact path={ROUTES.SELECTSTICKERS} component={SelectStickersPage} />
          <Route exact path={ROUTES.ADINFO} component={AdsInfo} />
          <Route exact path={ROUTES.HOME} component={HomePage} />
          <Route exact path={ROUTES.WISHLIST} component={WishListPage} />
        </div>
      </div>
    </Router>
  );
};

App.propTypes = {
  firebase: any,
  authUser: any,
};

export default withAuthentication(App);
