import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { withFirebase } from '../Firebase';
import * as ROUTES from '../../Constants/routes';
const withAuthorization = (condition) => (Component) => {
  class WithAuthorization extends React.Component {
    componentDidMount() {
      // eslint-disable-next-line react/prop-types
      this.listener = this.props.firebase.auth.onAuthStateChanged(
        (authUser) => {
          if (condition) {
            if (!condition(authUser)) {
              if (!authUser) {
                // eslint-disable-next-line react/prop-types
                this.props.history.push(ROUTES.LOGIN);
              } else {
                // eslint-disable-next-line react/prop-types
                this.props.history.push(ROUTES.COLLECTIONS);
              }
            }
          }
        }
      );
    }
    componentWillUnmount() {
      this.listener();
    }
    render() {
      return <Component {...this.props} />;
    }
  }
  return compose(withRouter, withFirebase)(WithAuthorization);
};
export default withAuthorization;
