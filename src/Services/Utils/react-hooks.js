import { useEffect, useState } from "react";

export const appUseEffect = useEffect;

export const appUseState = useState;
