/*eslint-disable */
import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyCsEFa8yJUA8pklFrNPs18LUORoVOtMY8s',
  authDomain: 'sticker-finder.firebaseapp.com',
  databaseURL: 'https://sticker-finder.firebaseio.com',
  projectId: 'sticker-finder',
  storageBucket: 'sticker-finder.appspot.com',
  messagingSenderId: '327437537734',
  appId: '1:327437537734:web:830ce972efacc7b03022c7',
  measurementId: 'G-HQHYFTQCVR',
};

class FirebaseApp {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.auth = firebase.auth();
    this.db = firebase.firestore();
    this.storage = firebase.storage();
  }

  // *** Auth API ***
  createUserWithEmailAndPassword(email, password) {
    return this.auth.createUserWithEmailAndPassword(email, password);
  }

  sendPasswordResetEmail(email) {
    return this.auth.sendPasswordResetEmail(email);
  }
  saveComment(obj) {
    return this.db.collection('comments').add(obj);
  }

  getCommantsId(userID) {
    return this.db
      .collection('comments')
      .where('idReceiver', '==', userID)
      .get();
  }

  deleteComment(commentId) {
    return this.db.collection('comments').doc(commentId).delete();
  }

  addUserToFirestore(user) {
    this.db.collection('Users').doc(user.id).set(user);
  }

  isUserAllowed(email) {
    this.db
      .collection('Users')
      .where('email', '==', email)
      .get()
      .then(function (userDb) {
        console.info('is allowed', !!userDb);
      });
  }

  loginWithEmailAndPassword(email, password) {
    return this.db
      .collection('Users')
      .where('email', '==', email)
      .get()
      .then(function (userDb) {
        if (userDb.docs[0].data().allowed) {
          return firebase.auth().signInWithEmailAndPassword(email, password);
        } else {
          throw new Error('Opss you have been block from our app');
        }
      });
  }

  logout() {
    return this.auth.signOut();
  }

  resetPassword(email) {
    return this.auth.sendPasswordResetEmail(email);
  }

  changePassword(password) {
    return this.auth.currentUser.updatePassword(password);
  }

  //upload image
  uploadImage(image, handleImageUpload, handleImageUploadError) {
    const uploadTask = this.storage.ref(`images/${image.name}`).put(image);
    uploadTask.on(
      'state_changed',
      (snapshot) => {
        // progress function ...
      },
      (error) => {
        // Error function ...
        if (handleImageUploadError) {
          handleImageUploadError(error);
        }
      },
      () => {
        // complete function ...
        this.storage
          .ref('images')
          .child(image.name)
          .getDownloadURL()
          .then((url) => {
            handleImageUpload(url);
          });
      }
    );
  }

  //Stickers
  getStickersByCollection(collectionId) {
    return this.db
      .collection('Stickers')
      .where('collectionId', '==', collectionId)
      .get();
  }

  addSticker(collectionId, obj) {
    return this.db.collection('Stickers').add(obj);
  }

  deleteSticker(stickerId) {
    return this.db.collection('Stickers').doc(stickerId).delete();
  }

  updateSticker(stickerId, obj) {
    return this.db.collection('Stickers').doc(stickerId).update(obj);
  }

  updateProfile(uid, obj) {
    return this.db.collection('Users').doc(uid).update(obj);
  }

  getStickerById(stickerId) {
    return this.db.collection('Stickers').doc(stickerId).get();
  }

  favSticker(userId, stickerId) {
    return this.db.collection('Likes').add({
      user: userId,
      sticker: stickerId,
    });
  }

  removeFavSticker(favId) {
    return this.db.collection('Likes').doc(favId).delete();
  }

  isFav(userId, stickerId) {
    return new Promise((resolve, reject) => {
      this.db
        .collection('Likes')
        .where('user', '==', userId)
        .where('sticker', '==', stickerId)
        .get()
        .then((likesDb) => {
          if (likesDb.docs.length) {
            resolve(likesDb.docs[0].id);
          } else {
            resolve(null);
          }
        })
        .catch((e) => reject(e));
    });
  }

  getFavStickers(userId) {
    return this.db.collection('Likes').where('user', '==', userId).get();
  }

  //User Collections

  addUserCollection(userId, collection, userCollections) {
    return this.db
      .collection('Users')
      .doc(userId)
      .update({ collections: [...userCollections, collection.uid] });
  }

  deleteUserCollection(userId, collection, userCollections) {
    const updatedCollections = userCollections.filter(
      (col) => col !== collection.uid
    );

    return this.db
      .collection('Users')
      .doc(userId)
      .update({ collections: updatedCollections });
  }

  getUserCollections(uid) {
    return this.db
      .collection('Users')
      .doc(uid)
      .get()
      .then(function (userInfo) {
        return userInfo.data().collections;
      });
  }
  //User stickers

  addUserSticker(userId, stickerId, userStickers) {
    return this.db
      .collection('Users')
      .doc(userId)
      .update({ stickers: [...userStickers, stickerId] });
  }

  deleteUserSticker(userId, updatedStickersList) {
    return this.db
      .collection('Users')
      .doc(userId)
      .update({ stickers: [...updatedStickersList] });
  }

  getUserStickers(uid) {
    return this.db
      .collection('Users')
      .doc(uid)
      .get()
      .then(function (userInfo) {
        return userInfo.data().stickers;
      });
  }

  //Liked Stickers

  addLikedSticker(userId, stickerId, userLikedStickers) {
    return this.db
      .collection('Users')
      .doc(userId)
      .update({ likedStickers: [...userLikedStickers, stickerId] });
  }

  getLikedUserStickers(uid) {
    return this.db
      .collection('Users')
      .doc(uid)
      .get()
      .then(function (userInfo) {
        return userInfo.data().likedStickers;
      });
  }

  deleteLikedSticker(userId, updatedLikedStickers) {
    return this.db
      .collection('Users')
      .doc(userId)
      .update({ likedStickers: [...updatedLikedStickers] });
  }

  //USER
  async getUser(uid) {
    const userInfo = await this.db.collection('Users').doc(uid).get();
    return userInfo.data();
  }

  //Collections
  getCollections() {
    return this.db.collection('Collections').get();
  }

  createCollection(obj) {
    return this.db.collection('Collections').add(obj);
  }

  deleteCollection(uid) {
    return this.db.collection('Collections').doc(uid).delete();
  }

  deleteAds(uid) {
    return this.db.collection('Ads').doc(uid).delete();
  }

  updateCollection(obj, id) {
    return this.db.collection('Collections').doc(id).update(obj);
  }

  getCollectionById(id) {
    return this.db.collection('Collections').doc(id).get();
  }

  getCollectionsByCategory(collectionFilter, subCollectionFilter) {
    return !subCollectionFilter
      ? this.db
          .collection('Collections')
          .where('collectionId', '==', collectionFilter)
          .get()
      : this.db
          .collection('Collections')
          .where('collectionId', '==', collectionFilter)
          .where('subCategoriesId', 'array-contains', subCollectionFilter)
          .get();
  }

  //Collection Categories - Dropdowns
  getCollectionCategories() {
    return this.db.collection('CollectionCategories').get();
  }

  searchCollectionByCategory(categoryId) {
    if (!categoryId) {
      return this.getCollections();
    }
    return this.db
      .collection('Collections')
      .where('collectionId', '==', categoryId)
      .get();
  }

  // Ads
  addAd(adData) {
    return this.db.collection('Ads').add(adData);
  }

  editAd(adId, adData) {
    return this.db.collection('Ads').doc(adId).set(adData);
  }

  getAdById(adId) {
    return this.db.collection('Ads').doc(adId).get();
  }

  getAdInfoById(adId) {
    return this.db
      .collection('Ads')
      .doc(adId)
      .get()
      .then(function (adInfo) {
        return adInfo.data();
      });
  }

  getAds(filters) {
    return new Promise(async (resolve, reject) => {
      try {
        let filtersQuery = this.db.collection('Ads');
        if (!filters) {
          const adsDb = await filtersQuery.get();
          resolve(adsDb);
          return;
        }
        let collectionsFromCategory = null;
        let { name, category, collection, sticker } = filters;
        if (sticker) {
          filtersQuery = filtersQuery.where('sticker', '==', sticker);
        } else if (collection) {
          filtersQuery = filtersQuery.where('collection', '==', collection);
        } else if (category) {
          collectionsFromCategory = [];
          const categoriesDb = await this.searchCollectionByCategory(category);
          categoriesDb.forEach((cat) => collectionsFromCategory.push(cat.id));
        }

        const adsDb = await filtersQuery.get();
        let res = [];
        adsDb.forEach((adDoc) => res.push(adDoc));
        if (name || collectionsFromCategory) {
          res = res.filter((adDoc) => {
            const ad = adDoc.data();
            if (name && ad.name.indexOf(name) < 0) {
              return false;
            }
            if (
              collectionsFromCategory &&
              collectionsFromCategory.indexOf(ad.collection) < 0
            ) {
              return false;
            }
            return true;
          });
        }
        resolve(res);
      } catch (error) {
        console.error(error);
        reject(error);
      }
    });
  }

  async deleteAdsOfSticker(stickerId) {
    const adsDb = await this.db
      .collection('Ads')
      .where('sticker', '==', stickerId)
      .get();
    const promises = [];

    adsDb.docs.forEach((adDoc) =>
      promises.push(this.db.collection('Ads').doc(adDoc.id).delete())
    );

    await Promise.all(promises);
  }

  // ADMIN USERS
  getUsersList() {
    const array = [];
    return this.db
      .collection('Users')
      .get()
      .then(function (usersDb) {
        usersDb.forEach(function (doc) {
          array.push({ uid: doc.id, ...doc.data() });
        });
        return array;
      });
  }

  updateUserState(uid, allowed) {
    return this.db.collection('Users').doc(uid).update({ allowed });
  }
}

export default FirebaseApp;
