import { createStyles } from "@material-ui/core";

const styles = () =>
  createStyles({
    root: {
      maxWidth: "100%",
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      height: "100%",
    },
    media: {
      height: 240,
    },
    header: {
      height: 80,
      fontSize: 30,
      padding: "0px 20px",
      backgroundColor: "#0085FC",
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-start",
    },
    headerTitle: {
      fontSize: 30,
      color: "white",
    },
    gridList: {
      width: 500,
      height: 450,
    },
    headerButton: {
      display: "inline-block",
      padding: "5px 9px",
      fontSize: "0.8125rem",
      minWidth: "64px",
      transition:
        "background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
      fontWeight: "500",
      borderRadius: "4px",
      textTransform: "uppercase",
      textAlign: "center",
      "&:hover": {
        cursor: "pointer",
        backgroundColor: "rgba(63, 81, 181, 0.04)",
      },
    },
    headerAddButton: {
      color: "#3f51b5",
      border: "1px solid rgba(63, 81, 181, 0.5)",
      "&:hover": {
        border: "1px solid #3f51b5",
      },
    },
    headerRemoveButton: {
      color: "#f05d5d",
      border: "1px solid rgba(240, 93, 93, 0.5)",
      "&:hover": {
        border: "1px solid #f05d5d",
      },
    },
  });

export default styles;
