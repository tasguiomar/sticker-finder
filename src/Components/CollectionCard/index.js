// React
// Material UI
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Typography,
  withStyles,
} from "@material-ui/core";
import CreateIcon from "@material-ui/icons/Create";
import DeleteIcon from "@material-ui/icons/Delete";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import { withFirebase } from "../../Services/Firebase";
import { withAuthentication } from "../../Services/Session";
// Styles
import styles from "./styles";

const CollectionCard = ({
  collection,
  classes,
  deleteCollection,
  editCollection,
  addUserCollection,
  isUser,
  userCanAdd,
  authUser,
  firebase,
  isAllowedToEdit,
  handleClick,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [isAdmin, setIsAdmin] = useState(false);

  const userId = authUser && authUser.uid;

  useEffect(() => {
    userId &&
      firebase.getUser(userId).then((user) => {
        setIsAdmin(user.admin);
      });
  }, [userId, firebase]);

  const openMenu = (event) => {
    setAnchorEl(event.currentTarget);
    event.stopPropagation();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const StyledMenu = withStyles({
    paper: {
      border: "1px solid #d3d4d5",
    },
  })((props) => (
    <Menu
      id="actionsMenu"
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      anchorEl={anchorEl}
      keepMounted
      open={Boolean(anchorEl)}
      onClose={handleClose}
      onClick={(e) => e.stopPropagation()}
      {...props}
    >
      <StyledMenuItem
        onClick={() => {
          editCollection(collection);
          handleClose();
        }}
      >
        <ListItemIcon>
          <CreateIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText primary="Edit" />
      </StyledMenuItem>
      <StyledMenuItem
        onClick={() => {
          deleteCollection(collection.uid);
          handleClose();
        }}
      >
        <ListItemIcon>
          <DeleteIcon fontSize="small" />
        </ListItemIcon>
        <ListItemText primary="Remove" size="small" color="danger" />
      </StyledMenuItem>
    </Menu>
  ));

  const StyledMenuItem = withStyles((theme) => ({
    root: {
      "&:focus": {
        backgroundColor: theme.palette.primary.main,
        "& .MuiListItemIcon-root, & .MuiListItemText-primary": {
          color: theme.palette.common.white,
        },
      },
    },
  }))(MenuItem);

  return (
    <Card className={classes.root}>
      <CardActionArea style={{ height: "fit-content" }}>
        <CardHeader
          style={{ width: "100%", justifyContent: "space-betweeen" }}
          aria-controls="actionsMenu"
          action={
            !isUser && userCanAdd ? (
              <div style={{ width: "100%", justifyContent: "space-betweeen" }}>
                <span
                  className={[classes.headerAddButton, classes.headerButton].join(" ")}
                  onClick={addUserCollection}
                >
                  ADD
                </span>
                {(isAdmin || isAllowedToEdit) && (
                  <MoreVertIcon onClick={openMenu} style={{ color: "#7EA6E0" }} />
                )}
              </div>
            ) : isUser ? (
              <div style={{ width: "100%", justifyContent: "space-betweeen" }}>
                <span
                  className={[classes.headerRemoveButton, classes.headerButton].join(" ")}
                  onClick={deleteCollection}
                >
                  REMOVE
                </span>
              </div>
            ) : (
              <div>
                {(isAdmin || isAllowedToEdit) && (
                  <MoreVertIcon onClick={openMenu} style={{ color: "#7EA6E0" }} />
                )}
              </div>
            )
          }
        />
        <CardMedia
          component="img"
          className={classes.media}
          image={collection.image}
          title="Collection #1"
          onClick={handleClick}
        />
        <CardContent onClick={handleClick}>
          <Typography gutterBottom variant="h5" component="h2">
            {collection.name}
          </Typography>
          <Typography variant="body2" style={{ color: "grey" }} component="p">
            {collection.subCategoriesId &&
              collection.subCategoriesId.map((subCategory) => `${subCategory}  `)}
          </Typography>
          <br />
          <Typography variant="body2" style={{ color: "black" }} component="p">
            {collection.description_short}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <br />
        <br />
        <br />
        <br />
        <Button size="small" style={{ color: "#7EA6E0" }} onClick={handleClick}>
          Read More
        </Button>
      </CardActions>
      <StyledMenu />
    </Card>
  );
};

CollectionCard.propTypes = {
  classes: any,
  collection: any,
  children: any,
  editCollection: any,
  deleteCollection: any,
  isUser: any,
  addUserCollection: any,
  userCanAdd: any,
  firebase: any,
  authUser: any,
  isAllowedToEdit: any,
  handleClick: any,
};

export default withFirebase(withAuthentication(withStyles(styles)(CollectionCard)));
