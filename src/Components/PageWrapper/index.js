// React
import React from 'react';

// Material UI
import { withStyles, Typography } from '@material-ui/core';

// Styles
import styles from './styles';

import { any } from 'prop-types';

const PageWrapper = ({ classes, children }) => {
  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <Typography className={classes.headerTitle}>STICKER FINDER</Typography>
      </div>
      <div className={classes.pageRoot}>{children}</div>
    </div>
  );
};

PageWrapper.propTypes = {
  classes: any,
  children: any
};

export default withStyles(styles)(PageWrapper);
