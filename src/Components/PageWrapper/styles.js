import { createStyles } from "@material-ui/core";
const styles = () =>
  createStyles({
    root: {
      display: "flex",
      flexDirection: "column",
      height: "100vh"
    },
    header: {
      height: 80,
      padding: "0px 20px",
      backgroundColor: "#0085FC",
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-start"
    },
    pageRoot: {
      display: "flex",
      height: "100%",
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#CCE5FF"
    },
    headerTitle: {
      fontSize: 30,
      color: "white"
    }
  });

export default styles;
