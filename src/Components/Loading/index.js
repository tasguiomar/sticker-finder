// Material ui
import { withStyles } from "@material-ui/core";
// React
import { any } from "prop-types";
import React from "react";
// Local
import styles from "./styles";

const LoadingComponent = ({ classes }) => (
  <div className={classes.loadingContainer}>
    <div className={classes.loading}></div>
  </div>
);

LoadingComponent.propTypes = {
  classes: any,
};

export default withStyles(styles)(LoadingComponent);
