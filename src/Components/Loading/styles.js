import { createStyles } from "@material-ui/core";
const blue = "#7EA6E0";
const styles = () =>
  createStyles({
    loadingContainer: {
      display: "flex",
      justifyContent: "center",
      height: "300px",
      width: "100%",
    },
    loading: {
      borderRadius: "50%",
      alignItems: "center",
      animation: `$spin 0.5s linear infinite`,
      background: `linear-gradient(90deg, rgba(204, 217, 255, 0.8) 85%, ${blue} 100%)`,
      display: "flex",
      height: "50px",
      justifyContent: "center",
      margin: "auto",
      width: "50px",
      "&:after": {
        content: '""',
        background: "white",
        borderRadius: "50%",
        height: "100%",
        transform: " scale(0.9)",
        width: "100%",
      },
    },
    "@keyframes spin": {
      "100%": {
        webkitTransform: "rotate(360deg)",
        transform: "rotate(360deg)",
      },
    },
    // new
  });

export default styles;
