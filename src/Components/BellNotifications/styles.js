import { createStyles } from "@material-ui/core";

const styles = () =>
  createStyles({
    bellContainer: {
      background: "var(--primary-color)",
      borderRadius: "50%",
      bottom: "20px",
      color: "white",
      cursor: "pointer",
      padding: "20px",
    },
    notificationItem: {
      padding: "5px 10px",
      "&:hover": {
        color: "var(--primary-color)",
        cursor: "pointer",
      },
    },
    notificationEmptyItem: {
      padding: "5px 10px",
    },
  });

export default styles;
