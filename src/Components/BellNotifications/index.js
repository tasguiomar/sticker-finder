import { Badge, Popover, withStyles } from "@material-ui/core";
import NotificationsIcon from "@material-ui/icons/Notifications";
import { any } from "prop-types";
import React, { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router";
import * as ROUTES from "../../Constants/routes";
import { withFirebase } from "../../Services/Firebase";
import { withAuthentication } from "../../Services/Session";
// Styles
import styles from "./styles";

function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

const BellNotification = ({ authUser, firebase, classes }) => {
  let history = useHistory();
  const [notifications, setNotifications] = useState([]);
  const [notificationsCode, setNotificationsCode] = useState(0);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const userId = authUser && authUser.uid;

  useInterval(() => {
    setNotificationsCode(notificationsCode + 1);
  }, 5000);

  useEffect(() => {
    if (userId) {
      firebase
        .getUser(userId)
        .then((userData) => {
          if (userData.notifications) {
            const newNotification = [];
            userData.notifications.forEach((ad) => newNotification.push(ad));
            setNotifications(newNotification);
          }
        })
        .catch((error) => console.error(error));
    }
  }, [firebase, userId, notificationsCode]);

  const handleOpenNotifications = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseNotifications = () => {
    if (userId) {
      firebase
        .getUser(userId)
        .then((userData) => {
          userData.notifications = [];
          return firebase.updateProfile(userId, userData);
        })
        .then(() => setNotificationsCode(notificationsCode + 1))
        .catch((error) => console.error(error));
    }

    setAnchorEl(null);
  };

  const handleNotificationClick = (adId) => {
    if (userId) {
      firebase
        .getUser(userId)
        .then((userData) => {
          userData.notifications = userData.notifications.filter(
            (notification) => notification.adId !== adId
          );
          return firebase.updateProfile(userId, userData);
        })
        .then(() => setNotificationsCode(notificationsCode + 1))
        .catch((error) => console.error(error));
    }

    setAnchorEl(null);
    history.push({
      pathname: ROUTES.ADINFO,
      state: adId,
    });
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  let popoverContent = notifications.length ? (
    notifications.map((notification) => (
      <div
        key={notification.adId}
        className={classes.notificationItem}
        onClick={() => handleNotificationClick(notification.adId)}
      >
        <span>{notification.adName}</span>
      </div>
    ))
  ) : (
    <div className={classes.notificationEmptyItem}>No notifications</div>
  );

  return (
    <div>
      <div className={classes.bellContainer} onClick={handleOpenNotifications}>
        <Badge aria-describedby={id} badgeContent={notifications.length} color="secondary">
          <NotificationsIcon />
        </Badge>
      </div>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleCloseNotifications}
        anchorOrigin={{
          vertical: "center",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
      >
        {popoverContent}
      </Popover>
    </div>
  );
};

BellNotification.propTypes = {
  firebase: any,
  classes: any,
  authUser: any,
};

export default withAuthentication(withFirebase(withStyles(styles)(BellNotification)));
