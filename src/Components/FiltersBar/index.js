// React
import { any } from "prop-types";
import React from "react";
import AdsFilters from "./Ads/index";
// Local
import CollectionFilters from "./Collections/index";
import StickerFilters from "./Stickers/index";

const FiltersBar = ({ type, handleFilter }) => {
  switch (type) {
    case "collection":
      return <CollectionFilters handleFilter={handleFilter} />;
    case "ad":
      return <AdsFilters handleFilter={handleFilter} />;
    default:
      return <StickerFilters handleFilter={handleFilter} />;
  }
};

/*type, handleFilter, */

FiltersBar.propTypes = {
  type: any,
  handleFilter: any,
};

export default FiltersBar;
