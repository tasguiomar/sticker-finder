// Material
import { withStyles, Select, TextField, Grid } from '@material-ui/core';
// React
import { any } from 'prop-types';
import React, { useEffect, useState } from 'react';
// Local
import styles from '../styles';
import { withFirebase } from '../../../Services/Firebase';
import LoadingComponent from '../../Loading';

const CollectionFilters = ({ classes, firebase, handleFilter }) => {
  const [loadingState, setLoadingState] = useState(true);
  const [collectionCategories, setCollectionCategories] = useState([]);
  const [collectionSubCategories, setCollectionSubCategories] = useState([]);

  const [selectedCategory, selectCategory] = useState('');
  const [selectedSubCategory, selectSubCategory] = useState('');
  const [searchText, setSearchText] = useState('');

  useEffect(() => {
    const array = [];
    firebase
      .getCollectionCategories()
      .then(function (collectionsDb) {
        collectionsDb.forEach(function (doc) {
          array.push({ uid: doc.id, ...doc.data() });
        });

        setCollectionCategories(array);
        setLoadingState(false);
      })
      .catch(() => setLoadingState(false));
  }, [firebase]);

  const handleChangeSelect = (event) => {
    event.preventDefault();
    const { name, value } = event.target;

    if (name === 'category') {
      selectCategory(value);
      !value && selectSubCategory('');
      handleFilter(value);

      collectionCategories.forEach((collection) => {
        if (collection.uid === value) {
          setCollectionSubCategories(collection.subCategories);
        }
      });
    }
    if (name === 'subCategory') {
      selectSubCategory(value);
      handleFilter(selectedCategory, value);
    }
  };

  const handleChangeText = (event) => {
    event.preventDefault();
    const { value } = event.target;

    setSearchText(value);
    handleFilter(selectedCategory, selectedSubCategory, value);
  };

  return (
    <div className={classes.headerContainer}>
      {loadingState ? (
        <LoadingComponent />
      ) : (
        <Grid container item spacing={2} xs={12} md={9}>
          <Grid item xs={12} md={3}>
            <TextField
              name={'textCategory'}
              value={searchText}
              className={classes.textInput}
              onChange={handleChangeText}
              label="Search by name"
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <Select
              native
              className={classes.textInput}
              value={selectedCategory}
              variant="outlined"
              onChange={handleChangeSelect}
              inputProps={{
                name: 'category',
                id: 'category',
              }}
            >
              <option aria-label={'empty'} value={''}>
                {'Select a Category'}
              </option>
              {collectionCategories.map((filter) => {
                return (
                  <option
                    key={filter.uid}
                    aria-label={filter.name}
                    value={filter.uid}
                  >
                    {filter.name}
                  </option>
                );
              })}
            </Select>
          </Grid>
          <Grid item xs={12} md={3}>
            <Select
              native
              disabled={!selectedCategory}
              variant="outlined"
              className={classes.textInput}
              value={selectedSubCategory}
              onChange={handleChangeSelect}
              inputProps={{
                name: 'subCategory',
                id: 'subCategory',
              }}
            >
              <option aria-label={'empty'} value={''}>
                {'Select a SubCategory'}
              </option>
              {collectionSubCategories.map((filter) => {
                return (
                  <option
                    key={filter.id}
                    aria-label={filter.name}
                    value={filter.id}
                  >
                    {filter.name}
                  </option>
                );
              })}
            </Select>
          </Grid>
        </Grid>
      )}
    </div>
  );
};

/*type, handleFilter, */
CollectionFilters.propTypes = {
  classes: any,
  title: any,
  handleFilter: any,
  firebase: any,
};

export default withFirebase(withStyles(styles)(CollectionFilters));
