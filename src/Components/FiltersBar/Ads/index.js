// Material
import { Grid, Select, TextField, withStyles } from "@material-ui/core";
// React
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
// Local
import { withFirebase } from "../../../Services/Firebase";
import AlertComponent from "../../Alert";
import LoadingComponent from "../../Loading";
import styles from "../styles";

const newFilters = (oldFilters, name, value) => {
  switch (name) {
    case "category":
      return {
        ...oldFilters,
        [name]: value,
        collection: "",
        sticker: "",
      };
    case "collection":
      return {
        ...oldFilters,
        [name]: value,
        sticker: "",
      };
    default:
      return {
        ...oldFilters,
        [name]: value,
      };
  }
};

const AdsFilters = ({ classes, firebase, handleFilter }) => {
  const [errorMessage, setErrorMessage] = useState(null);
  const [loadingState, setLoadingState] = useState(true);
  const [categories, setCategories] = useState([]);
  const [collections, setCollections] = useState([]);
  const [stickers, setStickers] = useState([]);
  const [adsFilters, setAdsFilters] = useState({
    name: "",
    category: "",
    collection: "",
    sticker: "",
  });

  useEffect(() => {
    const array = [];
    firebase
      .getCollectionCategories()
      .then(function (categoryDb) {
        categoryDb.forEach(function (doc) {
          array.push({ uid: doc.id, ...doc.data() });
        });

        setCategories(array);
        setLoadingState(false);
      })
      .catch((error) => {
        console.error(error);
        setErrorMessage("Could not load collection' categories on filters");
        setLoadingState(false);
      });
  }, [firebase]);

  useEffect(() => {
    const array = [];
    firebase
      .searchCollectionByCategory(adsFilters.category)
      .then(function (collectionsDb) {
        collectionsDb.forEach(function (doc) {
          array.push({ uid: doc.id, ...doc.data() });
        });

        setCollections(array);
        setLoadingState(false);
      })
      .catch((error) => {
        console.error(error);
        setErrorMessage("Could not load collections on filters");
        setLoadingState(false);
      });
  }, [firebase, adsFilters.category]);

  useEffect(() => {
    if (adsFilters.collection) {
      const array = [];
      firebase
        .getStickersByCollection(adsFilters.collection)
        .then(function (stickersDb) {
          stickersDb.forEach(function (doc) {
            array.push({ uid: doc.id, ...doc.data() });
          });

          setStickers(array);
          setLoadingState(false);
        })
        .catch((error) => {
          console.error(error);
          setErrorMessage("Could not load stickers on filters");
          setLoadingState(false);
        });
    }
  }, [firebase, adsFilters.collection]);

  const updateFilters = (name, value) => {
    const newAdsFilters = newFilters(adsFilters, name, value);
    setAdsFilters(newAdsFilters);
    handleFilter(newAdsFilters);
  };

  const handleChangeText = (event) => {
    event.preventDefault();
    updateFilters(event.target.name, event.target.value);
  };

  if (loadingState) {
    return <LoadingComponent />;
  }

  return (
    <div className={classes.headerContainer}>
      <Grid container item spacing={2} xs={12} md={9}>
        <Grid item xs={12} md={6} lg={3}>
          <TextField
            name="name"
            value={adsFilters.name}
            onChange={handleChangeText}
            label="Search by name"
            variant="outlined"
            className={classes.textInput}
          />
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Select
            native
            inputProps={{ name: "category" }}
            className={classes.textInput}
            value={adsFilters.category}
            variant="outlined"
            onChange={handleChangeText}
          >
            <option aria-label={"empty"} value={""}>
              {"Select a Category"}
            </option>
            {categories &&
              categories.map((filter) => {
                return (
                  <option key={filter.uid} aria-label={filter.name} value={filter.uid}>
                    {filter.name}
                  </option>
                );
              })}
          </Select>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Select
            native
            inputProps={{ name: "collection" }}
            className={classes.textInput}
            value={adsFilters.collection}
            variant="outlined"
            onChange={handleChangeText}
          >
            <option aria-label={"empty"} value={""}>
              {"Select a Collection"}
            </option>
            {collections &&
              collections.map((filter) => {
                return (
                  <option key={filter.uid} aria-label={filter.name} value={filter.uid}>
                    {filter.name}
                  </option>
                );
              })}
          </Select>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Select
            native
            inputProps={{ name: "sticker" }}
            className={classes.textInput}
            value={adsFilters.sticker}
            variant="outlined"
            onChange={handleChangeText}
            disabled={!adsFilters.collection}
          >
            <option aria-label={"empty"} value={""}>
              {"Select a Sticker"}
            </option>
            {stickers &&
              stickers.map((filter) => {
                return (
                  <option key={filter.uid} aria-label={filter.name} value={filter.uid}>
                    {filter.name}
                  </option>
                );
              })}
          </Select>
        </Grid>
      </Grid>
      <AlertComponent
        message={errorMessage}
        close={true}
        handleClose={() => setErrorMessage(null)}
      />
    </div>
  );
};

/*type, handleFilter, */
AdsFilters.propTypes = {
  classes: any,
  title: any,
  handleFilter: any,
  firebase: any,
};

export default withFirebase(withStyles(styles)(AdsFilters));
