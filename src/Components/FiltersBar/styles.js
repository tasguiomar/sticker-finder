import { createStyles } from "@material-ui/core";
const styles = () =>
  createStyles({
    headerContainer: { padding: "0px 10px" },
    textInput: {
      borderRadius: 10,
      width: "100%",
      "& .MuiOutlinedInput-root": {
        borderRadius: 10,
      },
    },
  });

export default styles;
