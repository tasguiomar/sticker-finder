// Material
import { withStyles, TextField, Grid } from '@material-ui/core';
// React
import { any } from 'prop-types';
import React, { useState } from 'react';
// Local
import styles from '../styles';
import { withFirebase } from '../../../Services/Firebase';

const StickerFilters = ({ classes, handleFilter }) => {
  const [searchText, setSearchText] = useState('');

  const handleChangeText = (event) => {
    event.preventDefault();
    const { value } = event.target;
    setSearchText(value);
    handleFilter(value);
  };

  return (
    <div className={classes.headerContainer}>
      <Grid container item spacing={2} xs={12} md={9}>
        <Grid item xs={12} md={3}>
          <TextField
            name={'textCategory'}
            value={searchText}
            onChange={handleChangeText}
            label="Search by name"
            variant="outlined"
            className={classes.textInput}
          />
        </Grid>
      </Grid>
    </div>
  );
};

/*type, handleFilter, */
StickerFilters.propTypes = {
  classes: any,
  title: any,
  handleFilter: any,
  firebase: any,
};

export default withFirebase(withStyles(styles)(StickerFilters));
