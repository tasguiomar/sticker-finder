import { createStyles } from '@material-ui/core';
const styles = theme =>
  createStyles({
    root: { display: 'flex', flexDirection: 'row' },
    mainMenu: {
      background: 'var(--primary-color)',
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      listStyle: 'none',
      padding: '5px 0',
      width: '100%',
      margin: '0'
    },
    menuHeader: {
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'center',
      height: '120px',
      paddingBottom: '10px',
      position: 'relative',
      '& h1': {
        color: 'var(--color-white)',
        fontSize: 'var(--font-size-lg)'
      }
    },
    menuHeaderEffect: {
      background: 'var(--color-white)',
      borderRadius: '50%',
      height: '350px',
      left: '0',
      opacity: '0.2',
      position: 'absolute',
      top: '0',
      transform: 'translate(-35%, -68%)',
      width: '500px',
      zIndex: '1'
    },
    menuBody: {
      flexGrow: '1'
    },
    menuFooter: {
      paddingTop: '10px'
    },
    menuItem: {
      display: 'inline-block',
      color: 'var(--color-white)',
      padding: '2px 0',
      position: 'relative',
      textDecoration: 'none',
      width: '100%',
      '&:hover,&.active': {
        background: 'rgba(255, 255, 255, 0.1)',
        cursor: 'pointer',
        '&:before': {
          background: 'var(--color-white)',
          content: "''",
          display: 'inline-block',
          height: '100%',
          position: 'absolute',
          width: '5px'
        }
      },
      '&:hover,&:active,&:visited': {
        color: 'var(--color-white)'
      }
    },
    menuItemContent: {
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'flex-start',
      overflow: 'hidden',
      padding: '5px 10px 5px 25px',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      '& span': {
        display: 'inline-block',
        marginLeft: '10px'
      }
    },
    [theme.breakpoints.down('sm')]: {
      menuHeader: {
        display: 'none'
      }
    }
  });

export default styles;
