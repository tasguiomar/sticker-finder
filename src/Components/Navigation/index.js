// React
// Material UI
import { withStyles } from '@material-ui/core';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import HomeIcon from '@material-ui/icons/Home';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ListIcon from '@material-ui/icons/List';
import { any } from 'prop-types';
import React, { useState, useEffect } from 'react';
import FavoriteIcon from '@material-ui/icons/Favorite';
// Routes
import { NavLink, withRouter } from 'react-router-dom';
// Constants
import * as ROUTES from '../../Constants/routes';
import { withFirebase } from '../../Services/Firebase';
// Context
import { withAuthentication } from '../../Services/Session';
import Alert from '../Alert';
// Styles
import styles from './styles';

const logoutRoute = {
  path: ROUTES.LOGIN,
  name: 'Logout',
  iconComponent: <ExitToAppIcon />,
};

const authRoutes = [
  { path: ROUTES.HOME, name: 'Home', iconComponent: <HomeIcon /> },
  {
    path: ROUTES.COLLECTIONS,
    name: 'Collections',
    iconComponent: <LibraryBooksIcon />,
  },
  {
    path: ROUTES.USERCOLLECTIONS,
    name: 'My Account',
    iconComponent: <AccountBoxIcon />,
  },
  {
    path: ROUTES.WISHLIST,
    name: 'Wish List',
    iconComponent: <FavoriteIcon />,
  },
  { path: ROUTES.ADS, name: 'Ads', iconComponent: <ListIcon /> },
  {
    path: ROUTES.OTHERUSERSPROFILE,
    name: 'Users',
    iconComponent: <SupervisorAccountIcon />,
  },
];

const adminRoutes = [
  { path: ROUTES.HOME, name: 'Home', iconComponent: <HomeIcon /> },
  {
    path: ROUTES.COLLECTIONS,
    name: 'Collections',
    iconComponent: <LibraryBooksIcon />,
  },
  {
    path: ROUTES.USERCOLLECTIONS,
    name: 'My Account',
    iconComponent: <AccountBoxIcon />,
  },
  {
    path: ROUTES.WISHLIST,
    name: 'Wish List',
    iconComponent: <FavoriteIcon />,
  },
  { path: ROUTES.ADS, name: 'Ads', iconComponent: <ListIcon /> },
  {
    path: ROUTES.ADMIN,
    name: 'Admin',
    iconComponent: <AccountCircle />,
  },
  {
    path: ROUTES.OTHERUSERSPROFILE,
    name: 'Users',
    iconComponent: <SupervisorAccountIcon />,
  },
];

const createMenuItem = (route, classes) => (
  <li key={route.path}>
    <NavLink
      to={route.path}
      activeClassName="active"
      className={classes.menuItem}
    >
      <div className={classes.menuItemContent}>
        {route.iconComponent}
        <span>{route.name}</span>
      </div>
    </NavLink>
  </li>
);

const createLogoutMenuItem = (route, classes, handleLogout) => (
  <li key={route.path}>
    <div className={classes.menuItem} onClick={handleLogout}>
      <div className={classes.menuItemContent}>
        {route.iconComponent}
        <span>{route.name}</span>
      </div>
    </div>
  </li>
);

const mapRoutes = (routes, classes) => {
  return routes.map((route) => {
    return createMenuItem(route, classes);
  });
};

const Navigation = ({ classes, authUser, firebase, history }) => {
  const [errorLogout, setErrorLogout] = useState(null);

  const [isAdmin, setUserAdmin] = useState(false);

  const userId = authUser && authUser.uid;

  useEffect(() => {
    userId &&
      firebase.getUser(userId).then((user) => {
        setUserAdmin(user.admin);
      });
  }, [userId, firebase]);

  const handleErrorClose = () => setErrorLogout(null);

  const signOut = () => {
    firebase.auth
      .signOut()
      .then(() => history.push(ROUTES.LOGIN))
      .catch(() =>
        setErrorLogout('Error trying to logout! Please try again later...')
      );
  };
  return (
    <ul className={classes.mainMenu}>
      <div className={classes.menuHeader}>
        <div className={classes.menuHeaderEffect}></div>
        <h1>Sticker Finder</h1>
      </div>
      <div className={classes.menuBody}>
        {mapRoutes(isAdmin ? adminRoutes : authRoutes, classes)}
      </div>
      <div className={classes.menuFooter}>
        {createLogoutMenuItem(logoutRoute, classes, signOut)}
      </div>
      <Alert
        type="error"
        message={errorLogout}
        close={true}
        handleClose={handleErrorClose}
      />
    </ul>
  );
};

Navigation.propTypes = {
  classes: any,
  authUser: any,
  firebase: any,
  history: any,
};

export default withRouter(
  withFirebase(withAuthentication(withStyles(styles)(Navigation)))
);
