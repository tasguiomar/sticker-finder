// Material
import { Button, withStyles } from '@material-ui/core';
// React
import { any } from 'prop-types';
import React, { useEffect, useState } from 'react';
// Local
import styles from './styles';
import { withAuthentication } from '../../Services/Session';
import { withFirebase } from '../../Services/Firebase';

const ListHeader = ({
  classes,
  title,
  filters,
  handleAdd,
  authUser,
  firebase,
  allowAll,
}) => {
  const handleAddClick = (e) => handleAdd && handleAdd(e);
  const [isAdmin, setIsAdmin] = useState(false);

  const userId = authUser && authUser.uid;

  useEffect(() => {
    userId &&
      firebase.getUser(userId).then((user) => {
        setIsAdmin(user.admin);
      });
  }, [userId, firebase]);

  return (
    <div className={classes.headerContainer}>
      <h1 className={classes.title}>
        {title} &nbsp;
        {(allowAll || isAdmin) && (
          <Button
            className={classes.addButton}
            size="small"
            onClick={handleAddClick}
          >
            Add new
          </Button>
        )}
      </h1>
      <div className={classes.filtersContainer}>Filters: {filters}</div>
    </div>
  );
};

ListHeader.propTypes = {
  classes: any,
  title: any,
  filters: any,
  handleAdd: any,
  firebase: any,
  authUser: any,
  allowAll: any,
};

export default withFirebase(withAuthentication(withStyles(styles)(ListHeader)));
