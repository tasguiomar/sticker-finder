import { createStyles } from "@material-ui/core";
const blue = "#7EA6E0";
const styles = () =>
  createStyles({
    headerContainer: {
      padding: "0 10px 10px",
    },
    title: {
      color: blue,
    },
    addButton: {
      color: blue,
    },
    // new
  });

export default styles;
