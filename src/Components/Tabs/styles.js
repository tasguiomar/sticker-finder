import { createStyles } from '@material-ui/core';
const styles = () =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      backgroundColor: '#DFDFDF',
      paddingTop: 20,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10
    },
    activeTab: {
      minWidth: 150,
      padding: '10px 20px',
      backgroundColor: 'white',
      color: '#000000',
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      border: '1px solid grey',
      borderLeft: 'none',
      borderBottom: 'none'
    },
    defaultTab: {
      minWidth: 150,
      padding: '10px 20px',
      backgroundColor: '#DFDFDF',
      color: '#0085FC',
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      border: '1px solid grey',
      borderLeft: 'none',
      borderBottom: 'none'
    },
    activeText: {
      fontSize: 20,
      textAlign: 'center',
      color: '#0085FC'
    },
    defaultText: { fontSize: 20, textAlign: 'center', color: '#000000' }
  });

export default styles;
