import React, { useState, useEffect } from 'react';
import { any } from 'prop-types';

// Constants
import * as ROUTES from '../../Constants/routes';
import { withStyles, Typography } from '@material-ui/core';
import styles from './styles';
import { useHistory } from 'react-router';

const tabsToSelect = [
  { path: ROUTES.LOGIN, name: 'Login' },
  { path: ROUTES.REGISTER, name: 'Register' },
];

const Tabs = ({ classes }) => {
  let history = useHistory();
  const [activeTab, setActiveTab] = useState(tabsToSelect[0]);

  useEffect(() => {
    if (history.location.pathname === ROUTES.LOGIN) {
      setActiveTab(tabsToSelect[0]);
    } else if (history.location.pathname === ROUTES.REGISTER) {
      setActiveTab(tabsToSelect[1]);
    }
  }, [history.location.pathname]);

  const handleSelectTab = (tab) => {
    history.location.pathname !== tab.path && history.push(tab.path);
  };

  return (
    <div className={classes.root}>
      {tabsToSelect.map((tab) => {
        return (
          <div
            onClick={() => handleSelectTab(tab)}
            className={
              activeTab.path === tab.path
                ? classes.activeTab
                : classes.defaultTab
            }
            key={tab.name}
          >
            <Typography
              variant={'h1'}
              className={
                activeTab.path === tab.path
                  ? classes.activeText
                  : classes.defaultText
              }
            >
              {tab.name}
            </Typography>
          </div>
        );
      })}
    </div>
  );
};

Tabs.propTypes = {
  classes: any,
};

export default withStyles(styles)(Tabs);
