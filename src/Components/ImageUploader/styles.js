import { createStyles } from '@material-ui/core';
const styles = () =>
  createStyles({
    image: {
      width: 200,
      maxhHeight: 200
    },
    imageGrid: {
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
      flexDirection: 'column'
    }
  });

export default styles;
