// React
import React, { useState } from 'react';

// Material UI
import { withStyles, Grid } from '@material-ui/core';

// Styles
import styles from './styles';
import { withFirebase } from '../../Services/Firebase';

import { any } from 'prop-types';

const PageWrapper = ({ classes, firebase, handleImageUpload, image }) => {
  const [imgUrl, setImgUrl] = useState(null);
  const handleChange = (e) => {
    if (e.target.files[0]) {
      const image = e.target.files[0];

      handleUpload(image);
    }
  };

  const handleImageUploadSuccess = (url) => {
    handleImageUpload(url);
    setImgUrl(url);
  };

  const handleUpload = (image) => {
    firebase.uploadImage(image, handleImageUploadSuccess);
  };

  return (
    <Grid item xs={12} className={classes.imageGrid}>
      <img
        alt={image ? 'fotografia' : ''}
        className={classes.image}
        src={image ? image : imgUrl}
      />
      <input type="file" onChange={handleChange} />
    </Grid>
  );
};

PageWrapper.propTypes = {
  classes: any,
  firebase: any,
  handleImageUpload: any,
  image: any,
};

export default withFirebase(withStyles(styles)(PageWrapper));
