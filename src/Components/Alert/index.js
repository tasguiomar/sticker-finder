import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import { any } from "prop-types";
import React from "react";
import getMessage from "./utils";

const AlertComponent = ({ close, handleClose, type, message, duration }) => {
  const dur = close ? duration || DEFAULT_DURATION : null;
  const closeHandler = close ? handleClose || (() => {}) : null;
  const msg = getMessage(message);
  return (
    <Snackbar open={!!message} autoHideDuration={dur} onClose={closeHandler}>
      <Alert onClose={closeHandler} severity={type}>
        {msg}
      </Alert>
    </Snackbar>
  );
};

AlertComponent.propTypes = {
  close: any,
  handleClose: any,
  type: any,
  message: any,
  duration: any,
};

const DEFAULT_DURATION = 6000;

export default AlertComponent;
