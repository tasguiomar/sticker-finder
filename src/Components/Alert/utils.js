const messageMap = {
  "Error: The email address is badly formatted.": "The account you entered is incorrect."
};

const getMessage = messageKey => {
  if (!messageKey) {
    return "";
  }
  return messageMap[messageKey] || messageKey.replace(/^Error: /, "");
};

export default getMessage;
