import { Tooltip } from "@material-ui/core";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import { any } from "prop-types";
import React, { useEffect, useState } from "react";
import { withFirebase } from "../../Services/Firebase";
import { withAuthentication } from "../../Services/Session";

const StickerListActions = ({
  handleDelClick,
  handleEditClick,
  handleAddUserSticker,
  handleDeleteUserSticker,
  handleLikeSticker,
  handleDislikeSticker,
  isUserStickersPage,
  isWishListPage,
  addButtonOn,
  sticker,
  classes,
  firebase,
  authUser,
}) => {
  const [isAdmin, setIsAdmin] = useState(false);
  const [favId, setFavId] = useState(null);
  const [isFav, setIsFav] = useState(false);

  const userId = authUser && authUser.uid;

  useEffect(() => {
    userId &&
      firebase.getUser(userId).then((user) => {
        setIsAdmin(user.admin);
      });

    firebase
      .isFav(userId, sticker.uid)
      .then((favId) => {
        setFavId(favId);
        setIsFav(!!favId);
      })
      .catch((error) => console.error(error));
  }, [userId, firebase, sticker, isFav]);

  const handleLocalFavClick = () => {
    handleLikeSticker && handleLikeSticker();
    setIsFav(true);
  };

  const handleLocalUnfavClick = () => {
    handleDislikeSticker && handleDislikeSticker(favId);
    setIsFav(false);
  };

  if (isUserStickersPage || isWishListPage) {
    let favUnfav = isFav ? (
      <FavoriteIcon
        title="Unlike Sticker"
        onClick={handleLocalUnfavClick}
        className="color-red"
      />
    ) : (
      <FavoriteBorderIcon
        title="Like Sticker"
        onClick={handleLocalFavClick}
        className="color-red"
      />
    );
    if (isUserStickersPage) {
      return (
        <div className={classes.listActions}>
          {addButtonOn(sticker.uid) && (
            <Tooltip title="Add to My stickers">
              <AddCircleOutlineIcon
                color="primary"
                onClick={handleAddUserSticker}
              />
            </Tooltip>
          )}
          {!addButtonOn(sticker.uid) && (
            <Tooltip title="Remove from My stickers">
              <RemoveCircleOutlineIcon
                color="primary"
                onClick={handleDeleteUserSticker}
              />
            </Tooltip>
          )}

          {favUnfav}
        </div>
      );
    }
    return <div className={classes.listActions}>{favUnfav}</div>;
  } else {
    return (
      <div className={classes.listActions}>
        {isAdmin && (
          <>
            <EditIcon title="Edit Sticker" onClick={handleEditClick} />
            <DeleteIcon title="Delete Sticker" onClick={handleDelClick} />
          </>
        )}
      </div>
    );
  }
};

StickerListActions.propTypes = {
  sticker: any,
  classes: any,
  handleEditClick: any,
  handleDelClick: any,
  handleAddUserSticker: any,
  handleDeleteUserSticker: any,
  handleLikeSticker: any,
  handleDislikeSticker: any,
  isWishListPage: any,
  addButtonOn: any,
  authUser: any,
  firebase: any,
};

export default withAuthentication(withFirebase(StickerListActions));
