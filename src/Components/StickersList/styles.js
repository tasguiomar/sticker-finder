import { createStyles } from '@material-ui/core';
const gray = '#aaaaaa';
const blue = '#7EA6E0';
const styles = (theme) =>
  createStyles({
    headerList: {
      display: 'none',
    },
    stickersList: {
      position: 'relative',
    },
    stickerItem: {
      margin: '2px auto 1px',
      padding: '20px 10px',
      overflow: 'hidden',
      position: 'relative',
      width: '100%',
    },
    row: {
      '& > div': {
        margin: 'auto',
        wordBreak: 'break-word',
        minHeight: '30px',
      },
    },
    imageContainer: {
      width: '100%',
      minHeight: '150px',
      height: '100%',
      overflow: 'hidden',
      position: 'relative',
      '& > img': {
        margin: 'auto',
        width: '100%',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
      },
    },
    listActions: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: '10px',
      position: 'absolute',
      bottom: '0',
      right: '0',
      '& > svg': {
        cursor: 'pointer',
        fontSize: '1.8rem',
      },
    },
    emptyMessage: {
      padding: '50px 20px',
      textAlign: 'center',
    },
    separator: {
      background: gray,
      border: 'none',
      color: gray,
      height: '1px',
      marginTop: 0,
      whiteSpace: 'nowrap',
    },
    showMoreLess: {
      color: blue,
      cursor: 'pointer',
      paddingLeft: '5px',
    },
    mobileHeaderLabel: {
      fontWeight: '700',
    },
    [theme.breakpoints.up('lg')]: {
      mobileHeaderLabel: {
        display: 'none',
      },
      headerList: {
        display: 'flex',
      },
      listActions: {
        position: 'static',
      },
    },
    [theme.breakpoints.between('sm', 'md')]: {
      stickersList: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr',
        gridTemplateRows: 'auto',
        columnGap: '12px',
        rowGap: '12px',
      },
    },
    // new
  });

export default styles;
