// Material
import { Card, Grid, withStyles } from "@material-ui/core";
import { any } from "prop-types";
// React
import React, { useState } from "react";
import { withRouter } from "react-router";
import EditSticker from "../../Pages/Collections/Stickers/EditSticker";
import AlertComponent from "../Alert";
import LoadingComponent from "../Loading";
import StickerListActions from "./StickerListActions";
// Local
import styles from "./styles";

const StickersListHeader = ({ classes }) => {
  return (
    <Grid container spacing={2} className={classes.headerList}>
      {StickersListItemLayout(null, 1 / 6)}
      {StickersListItemLayout("Name", 1 / 6)}
      {StickersListItemLayout("Code", 1 / 6)}
      {StickersListItemLayout("Description", 1 / 3)}
      {StickersListItemLayout(null, 1 / 6)}
      <Grid item xs={12}>
        <hr className={classes.separator} />
      </Grid>
    </Grid>
  );
};

const StickersListItemLayout = (info, percentage) => {
  const percentageVal = percentage || 1;
  const colsVal = 12 * percentageVal;
  return (
    <Grid item xs={12} lg={colsVal}>
      {info}
    </Grid>
  );
};

const LimitText = (text, classes) => {
  const [opened, setOpened] = useState(false);
  const maxLen = 200;
  const textHasMore = text && text.length > maxLen;
  const textValue = textHasMore ? text.substr(0, maxLen + 1) + (!opened ? "... " : "") : text;
  const textRest = textHasMore ? text.substr(maxLen + 1) + " " : "";

  const handleMoreLessInfo = () => setOpened(!opened);

  return (
    <span>
      {textValue}
      {opened ? textRest : ""}
      {textHasMore ? (
        <span className={classes.showMoreLess} onClick={handleMoreLessInfo}>
          {opened ? "show less" : "show more"}
        </span>
      ) : null}
    </span>
  );
};

const StickersListItem = ({ sticker, actions, classes }) => {
  return (
    <Card className={classes.stickerItem}>
      <Grid container spacing={2} className={classes.row}>
        {StickersListItemLayout(
          <div className={classes.imageContainer}>
            <img src={sticker.image} alt="sticker" />
          </div>,
          1 / 6
        )}
        {StickersListItemLayout(
          <div>
            <span className={classes.mobileHeaderLabel}>Name: </span>
            {sticker.name}
          </div>,
          1 / 6
        )}
        {StickersListItemLayout(
          <div>
            <span className={classes.mobileHeaderLabel}>Code: </span>
            {sticker.code}
          </div>,
          1 / 6
        )}
        {StickersListItemLayout(
          <div>
            <span className={classes.mobileHeaderLabel}>Description: </span>
            {LimitText(sticker.description, classes)}
          </div>,
          1 / 3
        )}
        {StickersListItemLayout(actions, 1 / 6)}
      </Grid>
    </Card>
  );
};

const StickersEmptyMessage = () => <Card className="empty-message">{EMPTY_LIST_MESSAGE}</Card>;

const StickersList = (props) => {
  const {
    handleItemClick,
    handleEditClick,
    handleDelClick,
    handleLikeSticker,
    handleDislikeSticker,
    handleAddUserSticker,
    isUserStickersPage,
    isWishListPage,
    handleDeleteUserSticker,
    addButtonOn,
    stickers,
    classes,
    error,
    loading,
    handleCloseEdit,
    setLoadingState,
    updateStickersList,
  } = props;

  if (loading) {
    return (
      <div>
        <StickersListHeader classes={classes} />
        <LoadingComponent />
      </div>
    );
  }

  if (error || !stickers.length) {
    return (
      <div>
        <StickersListHeader classes={classes} />
        <StickersEmptyMessage classes={classes} />
        <AlertComponent type="error" message={error} close={true} />
      </div>
    );
  }

  return (
    <div>
      <StickersListHeader classes={classes} />
      <div className={classes.stickersList}>
        {stickers.map((sticker) => {
          return sticker.toEdit ? (
            <EditSticker
              key={sticker.uid}
              setLoadingState={setLoadingState}
              sticker={sticker}
              handleClose={handleCloseEdit(sticker.uid)}
              updateStickersList={updateStickersList}
            />
          ) : (
            <StickersListItem
              key={sticker.uid}
              sticker={sticker}
              actions={
                <StickerListActions
                  handleEditClick={handleEditClick(sticker.uid)}
                  handleDelClick={handleDelClick(sticker.uid)}
                  handleAddUserSticker={handleAddUserSticker(sticker.uid)}
                  handleDeleteUserSticker={handleDeleteUserSticker(sticker.uid)}
                  handleLikeSticker={handleLikeSticker(sticker.uid)}
                  handleDislikeSticker={handleDislikeSticker}
                  isUserStickersPage={isUserStickersPage}
                  isWishListPage={isWishListPage}
                  addButtonOn={addButtonOn}
                  sticker={sticker}
                  classes={classes}
                />
              }
              onClick={handleItemClick(sticker.uid)}
              className={classes.stickerItem}
              {...props}
            />
          );
        })}
      </div>
    </div>
  );
};

StickersEmptyMessage.propTypes = {
  classes: any,
};

StickersListHeader.propTypes = {
  classes: any,
};

StickersListItem.propTypes = {
  sticker: any,
  actions: any,
  classes: any,
};

StickersList.propTypes = {
  stickers: any,
  error: any,
  loading: any,
  handleItemClick: any,
  handleEditClick: any,
  handleAddUserSticker: any,
  handleDeleteUserSticker: any,
  handleLikeSticker: any,
  handleDislikeSticker: any,
  addButtonOn: any,
  isUserStickersPage: any,
  isWishListPage: any,
  handleCloseEdit: any,
  handleDelClick: any,
  setLoadingState: any,
  classes: any,
  updateStickersList: any,
};

const EMPTY_LIST_MESSAGE = "The stickers list is empty";

export const StickersListTest = StickersList;

export default withRouter(withStyles(styles)(StickersList));
