import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import { any } from "prop-types";
import React, { useRef } from "react";

const ConfirmationModal = ({ onTrue, onFalse, title, message }) => {
  const open = useRef(true);

  return (
    <Dialog
      open={open.current}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title || "Are you sure?"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {message || "The item you are deleting cannot be restore after deleting it..."}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onFalse} color="primary" autoFocus>
          No
        </Button>
        <Button onClick={onTrue} color="secondary">
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ConfirmationModal.propTypes = {
  onTrue: any,
  onFalse: any,
  title: any,
  message: any,
};

export default ConfirmationModal;
